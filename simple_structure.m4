.PS
include(pstricks.m4)
include(libcct.m4)

scale=25.4

cct_init

dy=10
dx=10

mm=1

ri=50*mm
g=1*mm
hmi=5*mm
hmo=5*mm
h=10*mm

define(`midnight',`0.1, 0.1, 0.44')
define(`gold',`1,0.84,0')
define(`white',`1, 1, 1')
define(`copper',`0.85, 0.54, 0.40')
define(`ivory3',`0.80,0.80,0.75')
define(`black',`0,0,0')
define(`dark_gray',`0.45,0.45,0.45')
define(`lightblue',`0.53,0.808,0.98')
define(`lightgray',`0.69,0.769,0.871')
define(`lightred',`0.941,0.502,0.502')
define(`lightyellow',`1.0,1.0,0.8')

\newgray{bwhite}{1}
\newgray{bblack}{0}
\newrgbcolor{bgray}{0.69 0.769 0.871}

pi = atan2(0,-1)
radius_ = ((h/2)/3)*0.9

define(`I_cross',`[\
  I: circle rad radius_ fill 1;
  line from 1/5 between I.ne and I.sw to 4/5 between I.ne and I.sw;
  line from 1/5 between I.nw and I.se to 4/5 between I.nw and I.se]' )

define(`I_dot',`[\
  I: circle rad radius_ fill 1;
  command "`\pscustom[fillstyle=solid]{'"
    circle rad 2/5 with .c at I.c
    circle rad 1/5 with .c at I.c
    circle rad 0.1 with .c at I.c
  command "`}%'"]' )
  


p = 14
kq = 0.5
coilTotal = kq * 3 * p
coilPitch = (360)/coilTotal
magnetTotal = 4*p
magnetPitch = (360)/magnetTotal

kc = 0.55
coil_side_angle = 0.5*kc*coilPitch
coil_block_angle = (1-kc)*coilPitch

start_ang=90-4*magnetPitch
end_ang=90+4*magnetPitch

O: (0,0)

arcd(O,ri,start_ang,end_ang) ccw
arcd(O,ri+hmi,start_ang,end_ang) ccw
#arcd(O,ri+hmi+g,start_ang,end_ang) ccw
#arcd(O,ri+hmi+g+h,start_ang,end_ang) ccw
arcd(O,ri+hmi+g+h+g,start_ang,end_ang) ccw
arcd(O,ri+hmi+g+h+hmo+g,start_ang,end_ang) ccw

#darrow init

#darc(center position, radius, start radians, end radians, dline thickness, arrowhead wid, arrowhead
#ht, terminals)

#darc(O, ri-10, start_ang, end_ang, linewid, 1, <-, 0)

##Flux Amplitude/Frequency etc.
#_Af=1         #Amplitude
#_f=50	        #Frequency [Hz]
#_Tf=1/_f      #Period
#_wf=2*pi*_f   #Frequency [rad/s]
#_cycles=2
#_xscale=140*mm/(_cycles*_Tf)   #X-axis scaling factor [i.e. 150mm wide]
#_yscale=40*mm/_Af              #Y-axis scaling factor [i.e.50mm high]
#_K_p=2/3	# Pole-arc to Pole-pitch ratio
#_offset=8*mm	# Magnet offset [mm]
#_thick=6*mm	# Magnet thickness/height

#box wid _K_p*_Tf/2*_xscale ht _thick with .nw at ((_K_p*_Tf/8+_Tf)*_xscale,-_offset)

#line from O to Rect_(ri+hmi+h+hmo,end_ang+5) ->
#"$R_o$" at last line .end above rjust

#DRAW DIMENSION LABELS
#dimension (linespec,offset,label, D|H|W|blank width,tic offset,arrowhead )
setrgb(dark_gray)

#dimension_(from O to (Rect_(ri,(end_ang+start_ang)/2)),0,"$r_i$", dx,5,->)
dimension_(from (Rect_(0.5*ri,(end_ang+start_ang)/2)) to (Rect_(ri,(end_ang+start_ang)/2)),0,"$r_i$", dx,5,->)

temp = 10
#dimension_(from O to (Rect_(ri+hmi+h+hmo+2*g,end_ang+temp)),0,"$r_o$", dx,5,->)
dimension_(from (Rect_(0.47*(ri+hmi+h+hmo+2*g),end_ang+temp+5)) to (Rect_(ri+hmi+h+hmo+2*g,end_ang+temp)),0,"$r_o$", dx,5,->)
arcd(O,ri+hmi+h+hmo+2*g,end_ang+0.6*temp,end_ang+1.4*temp) ccw

#dimension_(from (Rect_(ri+hmi+g+h+g,start_ang)) to (Rect_(ri+hmi+g+h+hmo+g,start_ang)),-10,"$h_{mo}$", dx,7,<->)
temp = 10
middle = 1.1*temp
arc_width = 0.35
line from Rect_(ri+hmi+g+h+g+hmo+0.8*hmo,start_ang-middle) to Rect_(ri+hmi+g+h+g+hmo,start_ang-middle) ->
line from Rect_(ri+hmi+g+h+g-0.8*hmo,start_ang-middle) to Rect_(ri+hmi+g+h+g,start_ang-middle) ->
"$h_{mo}$"at -0.1 between last line .end and 2nd last line above ljust
arcd(O,ri+hmi+g+h+g,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
arcd(O,ri+hmi+g+h+g+hmo,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#dimension_(from (Rect_(ri,start_ang)) to (Rect_(ri+hmi,start_ang)),-10,"$h_{mi}$", dx,7,<->)
temp = 10
middle = 1.1*temp
arc_width = 0.35
line from Rect_(ri+hmi+0.8*hmo,start_ang-middle) to Rect_(ri+hmi,start_ang-middle) ->
line from Rect_(ri-0.8*hmo,start_ang-middle) to Rect_(ri,start_ang-middle) ->
"$h_{mi}$"at -0.1 between last line .end and 2nd last line above ljust
arcd(O,ri+hmi,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
arcd(O,ri,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#dimension_(from (Rect_(ri+hmi+h+hmo,start_ang)) to (Rect_(ri+hmi+h+hmo,start_ang+magnetPitch)),-5,"$k_{m}\frac{\pi}{p}$", dx,0,<->)
line from Rect_(ri+hmi+g+h+g+hmo,start_ang) to Rect_(ri+hmi+g+h+g+hmo+0.8*hmo,start_ang)
line from Rect_(ri+hmi+g+h+g+hmo,start_ang+magnetPitch) to Rect_(ri+hmi+g+h+g+hmo+0.8*hmo,start_ang+magnetPitch)
#"$k_{mo}\frac{\pi}{p}$"at 0 between last line .end and 2nd last line center ljust
"$k_{m}\frac{\pi}{p}$"at 0 between last line .end and 2nd last line center ljust
dimension_(from (Rect_(ri+hmi+g+h+g+hmo+0.4*hmo,start_ang)) to (Rect_(ri+hmi+g+h+g+hmo+0.4*hmo,start_ang+magnetPitch)),0,"", dx,0,<->)
#arcd(O,ri+hmi+g+h+g+hmo+0.4*hmo,start_ang-0.3*temp,start_ang) ccw ->

line from Rect_(ri,start_ang) to Rect_(ri-0.8*hmo,start_ang)
line from Rect_(ri,start_ang+magnetPitch) to Rect_(ri-0.8*hmo,start_ang+magnetPitch)
#"$k_{mi}\frac{\pi}{p}$"at 0.8 between last line .end and 2nd last line below rjust
"$k_{m}\frac{\pi}{p}$"at 0.8 between last line .end and 2nd last line below rjust
dimension_(from (Rect_(ri-0.4*hmo,start_ang)) to (Rect_(ri-0.4*hmo,start_ang+magnetPitch)),0,"", dx,0,<->)

#dimension_(from (Rect_(ri+hmi+g,end_ang)) to (Rect_(ri+hmi+g+h,end_ang)),5,"$h_{c}$", dx,-3,<->)
temp = 10
middle = 0.5*temp
line from Rect_(ri+hmi+g+h+0.8*hmo,end_ang+middle) to Rect_(ri+hmi+g+h,end_ang+middle) ->
line from Rect_(ri+hmi+g-0.8*hmo,end_ang+middle) to Rect_(ri+hmi+g,end_ang+middle) ->
"$h_{c}$" at 0.20 between last line .end and 2nd last line above rjust
arcd(O,ri+hmi+g,end_ang+0.5*middle,end_ang+1.5*middle) ccw
arcd(O,ri+hmi+g+h,end_ang+0.5*middle,end_ang+1.5*middle) ccw

dimension_(from (Rect_(ri+hmi+g+h/2,start_ang+coil_side_angle)) to (Rect_(ri+hmi+g+h/2,start_ang+coil_side_angle+coil_block_angle)),0,"", 0,0,<->)
"$k_{c}$" at 0.75 between last line .end and 2nd last line above

dimension_(from (Rect_(ri+hmi,start_ang)) to (Rect_(ri+hmi+g,start_ang)),-2,"", dx,0,<->)
#"$g_i$" at last line .end below ljust
"$g$" at last line .end below ljust

dimension_(from (Rect_(ri+hmi+g+h,start_ang)) to (Rect_(ri+hmi+g+h+g,start_ang)),-2,"", dx,0,<->)
#"$g_o$" at last line .end below ljust
"$g$" at last line .end below ljust



setrgb(black)
#DRAW RADIAL LINES FOR INNER MAGNETS AND OUTER MAGNETS
for i = start_ang to end_ang do{
  line from Rect_(ri+hmi+g+h+g,i) to Rect_(ri+hmi+g+h+hmo+g,i)#outer rotor
  line from Rect_(ri,i) to Rect_(ri+hmi,i)#inner rotor
  i = i + magnetPitch - 1
  
}

#FILL OUTER MAGNETS WITH COLOR
outer_magnet_ri = ri+hmi+g+h+g
outer_magnet_ro = ri+hmi+g+h+hmo+g

magnet_ri = outer_magnet_ri
magnet_ro = outer_magnet_ro

#downfacing magnets
start_tmp = start_ang
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightblue,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

#upfacing magnets
start_tmp = start_ang+2*magnetPitch
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightred,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

#sidefacing magnets
start_tmp = start_ang+1*magnetPitch
end_tmp = start_tmp+magnetPitch
for i = 0 to 3 do{
  rgbfill(lightyellow,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 2*magnetPitch
  end_tmp = end_tmp + 2*magnetPitch
}

#FILL INNER MAGNETS WITH COLOR
inner_magnet_ri = ri
inner_magnet_ro = ri+hmi

magnet_ri = inner_magnet_ri
magnet_ro = inner_magnet_ro

#downfacing magnets
start_tmp = start_ang
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightblue,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

#upfacing magnets
start_tmp = start_ang+2*magnetPitch
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightred,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

#sidefacing magnets
start_tmp = start_ang+1*magnetPitch
end_tmp = start_tmp+magnetPitch
for i = 0 to 3 do{
  rgbfill(lightyellow,
  line from Rect_(magnet_ro,start_tmp) to Rect_(magnet_ri,start_tmp)
  arcd(O,magnet_ro,start_tmp,end_tmp) ccw
  line from Rect_(magnet_ri,end_tmp) to Rect_(magnet_ro,end_tmp)
  arcd(O,magnet_ri,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 2*magnetPitch
  end_tmp = end_tmp + 2*magnetPitch
}



#DRAW RADIAL AND TANGETAL LINES FOR STATOR COILS
j = 0
add = 0
for i = start_ang to end_ang do{
  temp = j%3
  if temp == 0 then {
    rgbfill(copper,
      line from Rect_(ri+hmi+g,i) to Rect_(ri+hmi+h+g,i)
      add = coil_side_angle
      arcd(O,ri+hmi+g+h,i+add,i) cw
      line from Rect_(ri+hmi+h+g,i+add) to Rect_(ri+hmi+g,i+add)
      arcd(O,ri+hmi+g,i,i+add) ccw
    )
  }
  if temp == 1 then {
    #rgbfill(white,
      line from Rect_(ri+hmi+g,i) to Rect_(ri+hmi+h+g,i)
      add = coil_block_angle
      arcd(O,ri+hmi+g+h,i,i+add) ccw
      #line from Rect_(ri+hmi+h+g,i+add) to Rect_(ri+hmi+g,i+add)
      arcd(O,ri+hmi+g,i+add,i) cw
    #)
  }
  if temp == 2 && i+add-1 < end_ang then {
    rgbfill(copper,
      line from Rect_(ri+hmi+g,i) to Rect_(ri+hmi+h+g,i)
      add = coil_side_angle
      arcd(O,ri+hmi+g+h,i+add,i) cw
      line from Rect_(ri+hmi+h+g,i+add) to Rect_(ri+hmi+g,i+add)
      arcd(O,ri+hmi+g,i+add,i) cw
    )    
  }
  
  
  i = i + add
  j = j + 1
  i = i - 1
}
add = coil_side_angle
rgbfill(copper,
  line from Rect_(ri+hmi+g,end_ang-add) to Rect_(ri+hmi+h+g,end_ang-add)
  arcd(O,ri+hmi+h+g,end_ang,end_ang-add) cw
  line from Rect_(ri+hmi+h+g,end_ang) to Rect_(ri+hmi+g,end_ang)
  arcd(O,ri+hmi+g,end_ang-add,end_ang) ccw
)


#line from Rect_(ri+hmi/2,start_ang) to Rect_(ri+hmi/2,start_ang+magnetPitch) ->

#setrgb(ivory3)
#DRAW INNER MAGNET ORIENTATION ARROWS
j = 0
for i = start_ang+(magnetPitch/4) to end_ang do{
  temp = j%4
  
  if temp == 0 then {
    line from Rect_(ri+0.2*hmi,i+magnetPitch/4) to Rect_(ri+0.85*hmi,i+magnetPitch/4) <-
  }  
  if temp == 1 then {
    arcd(O,ri+hmi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw ->
  }
  if temp == 2 then {
    line from Rect_(ri+0.2*hmi,i+magnetPitch/4) to Rect_(ri+0.85*hmi,i+magnetPitch/4) ->
  }
  if temp == 3 then {
    arcd(O,ri+hmi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  }  
  
  i = i - 1 + magnetPitch
  j = j + 1
}

#DRAW OUTER MAGNET ORIENTATION ARROWS
j = 0
for i = start_ang+(magnetPitch/4) to end_ang do{
  temp = j%4
  
  if temp == 0 then {
    line from Rect_(ri+hmi+g+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hmi+g+h+g+0.85*hmo,i+magnetPitch/4) <-
  }  
  if temp == 1 then {
    arcd(O,ri+hmi+g+h+g+hmo/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  }
  if temp == 2 then {
    line from Rect_(ri+hmi+g+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hmi+g+h+g+0.85*hmo,i+magnetPitch/4) ->
  }
  if temp == 3 then {
    arcd(O,ri+hmi+g+h+g+hmo/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw ->
  }  
  
  i = i - 1 + magnetPitch
  j = j + 1
}

#DRAW STATOR COIL CONDUCTORS
j = 0
add = 0
for i = start_ang to end_ang do{
  temp = j%3
  if temp == 0 then {
    add = coil_side_angle
    I_cross with .c at ((Rect_(ri+hmi+g+0.8*h,i+coil_side_angle/2)))
    I_cross with .c at ((Rect_(ri+hmi+g+0.5*h,i+coil_side_angle/2)))
    I_cross with .c at ((Rect_(ri+hmi+g+0.2*h,i+coil_side_angle/2)))
  }
  if temp == 0 then {
    add = coil_block_angle
  }  
  if temp == 2 && i+add-1 < end_ang then {
    add = coil_side_angle
    I_dot with .c at ((Rect_(ri+hmi+g+0.8*h,i + coil_block_angle/2 - coil_side_angle/2)))
    I_dot with .c at ((Rect_(ri+hmi+g+0.5*h,i + coil_block_angle/2 - coil_side_angle/2)))
    I_dot with .c at ((Rect_(ri+hmi+g+0.2*h,i + coil_block_angle/2 - coil_side_angle/2)))
  }
  
  
  i = i + add - 1
  j = j + 1
  i = i - 1
}
#add = coil_side_angle
I_dot with .c at ((Rect_(ri+hmi+g+0.8*h,end_ang - coil_side_angle/2)))
I_dot with .c at ((Rect_(ri+hmi+g+0.5*h,end_ang - coil_side_angle/2)))
I_dot with .c at ((Rect_(ri+hmi+g+0.2*h,end_ang - coil_side_angle/2)))


#line from O to Rect_(ri,end_ang) ->
#"$R_i$" at last line .end above rjust

#"test" at 1/3 between last line .end and 2nd last arc .c



.PE