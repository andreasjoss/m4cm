.PS
include(pstricks.m4)
include(libcct.m4)

scale=25.4

cct_init

dy=10
dx=10

mm=1

ri=50*mm
g=1*mm
hyi=5*mm
hyo=7*mm
hmo=5*mm
h=10*mm

define(`midnight',`0.1, 0.1, 0.44')
define(`gold',`1,0.84,0')
define(`white',`1, 1, 1')
define(`copper',`0.85, 0.54, 0.40')
define(`ivory3',`0.80,0.80,0.75')
define(`black',`0,0,0')
define(`dark_gray',`0.45,0.45,0.45')
define(`lightblue',`0.53,0.808,0.98')
define(`lightgray',`0.69,0.769,0.871')
define(`lightred',`0.941,0.502,0.502')

\newgray{bwhite}{1}
\newgray{bblack}{0}
#\newgray{bgray}{0.69}{0.769}{0.871}
\newrgbcolor{bgray}{0.69 0.769 0.871}

pi = atan2(0,-1)
#radius_ = ((h/2)/3)*0.9
radius_ = 0

define(`I_cross',`[\
  I: circle rad radius_ fill 1;
  line from 1/5 between I.ne and I.sw to 4/5 between I.ne and I.sw;
  line from 1/5 between I.nw and I.se to 4/5 between I.nw and I.se]' )

define(`I_dot',`[\
  I: circle rad radius_ fill 2;
  command "`\pscustom[fillstyle=solid]{'"
    #circle rad 2/5 with .c at I.c
    circle rad 1/5 with .c at I.c
    circle rad 0.1 with .c at I.c
  command "`}%'"]' )
  


p = 14
kq = 0.5
coilTotal = kq * 3 * p
coilPitch = (360)/coilTotal
magnetTotal = 4*p
magnetPitch = (360)/magnetTotal

kc = 0.55
coil_side_angle = 0.5*kc*coilPitch
coil_block_angle = (1-kc)*coilPitch

#start_ang=63
#end_ang=117#start_ang+8*magnetPitch

start_ang=90-1*magnetPitch
end_ang=90+1*magnetPitch

O: (0,0)

#arcd(O,ri,start_ang,end_ang) ccw
#arcd(O,ri+hyi,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g+h,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g+h+g,start_ang,end_ang) ccw

#FILL COLOR FOR STATOR TEETH
start_tmp = start_ang + coil_side_angle
end_tmp = start_tmp+coil_block_angle
\psset{linecolor=bwhite,fillcolor=bwhite}
#for i = (start_ang+coil_side_angle) to (end_ang do-coil_side_angle){
for i = 0 to 0 do{
  rgbfill(lightgray,
  #line from Rect_(ri,start_tmp) to Rect_(ri+hyi+h,start_tmp)
  line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  #arcd(O,ri+hyi+h,end_tmp,start_tmp) cw
  #line from Rect_(ri+hyi+h,end_tmp) to Rect_(ri,end_tmp)
  line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  #arcd(O,ri,start_tmp,end_tmp) ccw
  arcd(O,ri,end_tmp,start_tmp) cw
  )
  start_tmp = start_tmp+coilPitch
  end_tmp = end_tmp+coilPitch  
}

#draw lines which close inner yoke
#\psset{linecolor=bwhite,fillcolor=bwhite}
\psset{linecolor=bgray,fillcolor=bgray}
rgbfill(lightgray,
arcd(O,ri,start_ang,end_ang) ccw
#arcd(O,ri,end_ang,start_ang) cw
#line from Rect_(ri,end_ang) to Rect_(ri+hyi,end_ang)
line from Rect_(ri+hyi,end_ang) to Rect_(ri,end_ang)
#arcd(O,ri+hyi,start_ang,end_ang) ccw
arcd(O,ri+hyi,end_ang,start_ang) cw
line from Rect_(ri,start_ang) to Rect_(ri+hyi,start_ang)
)

#draw lines which close outer yoke
\psset{linecolor=bwhite,fillcolor=bwhite}
rgbfill(lightgray,
#rgbfill(copper,
arcd(O,ri+hyi+h+hmo+g,start_ang,end_ang) ccw
#\psset{linecolor=bwhite,fillcolor=bwhite}
line from Rect_(ri+hyi+h+g+hmo,end_ang) to Rect_(ri+hyi+h+g+hmo+hyo,end_ang)
#\psset{linecolor=bblack,fillcolor=bblack}
arcd(O,ri+hyi+h+hmo+g+hyo,end_ang,start_ang) cw
#\psset{linecolor=bwhite,fillcolor=bwhite}
line from Rect_(ri+hyi+h+g+hmo,start_ang) to Rect_(ri+hyi+h+g+hmo+hyo,start_ang)
)
#\psset{linecolor=bblack,fillcolor=bblack}

#draw lines which should remain black
\psset{linecolor=bblack,fillcolor=bblack}
arcd(O,ri,start_ang,end_ang) ccw
arcd(O,ri+hyi+h+hmo+g,start_ang,end_ang) ccw
arcd(O,ri+hyi+h+hmo+g+hyo,end_ang,start_ang) cw

\psset{linecolor=bblack,fillcolor=bblack}


#DRAW DIMENSION LABELS
#setrgb(dark_gray)

#dimension_(from (Rect_(0.5*ri,(end_ang+start_ang)/2)) to (Rect_(ri,(end_ang+start_ang)/2)),0,"$r_i$", dx,5,->)

#temp = 10
#dimension_(from (Rect_(0.47*(ri+hyi+h+hmo+g),end_ang+temp+5)) to (Rect_(ri+hyi+h+hmo+g+hyo,end_ang+temp)),0,"$r_o$", dx,5,->)
#arcd(O,ri+hyi+h+hmo+g+hyo,end_ang+0.6*temp,end_ang+1.4*temp) ccw

#temp = 10
#middle = 1.1*temp
#arc_width = 0.35
#line from Rect_(ri+hyi+h+g+hmo+0.8*hmo,start_ang-middle) to Rect_(ri+hyi+h+g+hmo,start_ang-middle) ->
#line from Rect_(ri+hyi+h+g-0.8*hmo,start_ang-middle) to Rect_(ri+hyi+h+g,start_ang-middle) ->
#"$h_{mo}$"at -0.1 between last line .end and 2nd last line above ljust
#arcd(O,ri+hyi+h+g,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
#arcd(O,ri+hyi+h+g+hmo,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#temp = 10
#middle = 1.1*temp
#arc_width = 0.35
#line from Rect_(ri+hyi+0.8*hmo,start_ang-middle) to Rect_(ri+hyi,start_ang-middle) ->
#line from Rect_(ri-0.8*hmo,start_ang-middle) to Rect_(ri,start_ang-middle) ->
#"$h_{yi}$"at -0.1 between last line .end and 2nd last line above ljust
#arcd(O,ri+hyi,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
#arcd(O,ri,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#line from Rect_(ri+hyi+h+g+hmo,start_ang+magnetPitch/2) to Rect_(ri+hyi+h+g+hmo+0.3*hmo,start_ang+magnetPitch/2)
#line from Rect_(ri+hyi+h+g+hmo,start_ang+magnetPitch+magnetPitch/2) to Rect_(ri+hyi+h+g+hmo+0.3*hmo,start_ang+magnetPitch+magnetPitch/2)
#"$k_{m}\frac{\pi}{p}$"at 0 between last line .end and 2nd last line center ljust
#dimension_(from (Rect_(ri+hyi+h+g+hmo+0.4*hmo,start_ang+magnetPitch/2)) to (Rect_(ri+hyi+h+g+hmo+0.4*hmo,start_ang+magnetPitch+magnetPitch/2)),0,"", dx,0,<->)

#temp = 10
#middle = 0.5*temp
#line from Rect_(ri+hyi+h+0.8*hmo,end_ang+middle) to Rect_(ri+hyi+h,end_ang+middle) ->
#line from Rect_(ri+hyi-0.8*hmo,end_ang+middle) to Rect_(ri+hyi,end_ang+middle) ->
#"$h_{c}$" at 0.20 between last line .end and 2nd last line above rjust
#arcd(O,ri+hyi,end_ang+0.5*middle,end_ang+1.5*middle) ccw
#arcd(O,ri+hyi+h,end_ang+0.5*middle,end_ang+1.5*middle) ccw

#middle = -2*magnetPitch
#line from Rect_(ri+hyi+h+g+hmo,end_ang+middle) to Rect_(ri+hyi+h+g+hmo+hyo,end_ang+middle) <->
#"$h_{yo}$" at 0.26 between last line .end and 2nd last line above

#dimension_(from (Rect_(ri+hyi+h/2,start_ang+coil_side_angle)) to (Rect_(ri+hyi+h/2,start_ang+coil_side_angle+coil_block_angle)),0,"", 0,0,<->)
#"$k_{c}$" at 0.75 between last line .end and 2nd last line above

#dimension_(from (Rect_(ri+hyi+h,start_ang)) to (Rect_(ri+hyi+h+g,start_ang)),-2,"", dx,0,<->)
#"$g$" at last line .end below ljust


setrgb(black)


  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  #arcd(O,ri,end_tmp,start_tmp) cw

#FILL OUTER MAGNETS WITH COLOR
#downfacing magnets
start_tmp = start_ang+magnetPitch/2
end_tmp = start_tmp+magnetPitch
for i = 0 to 0 do{
  rgbfill(lightblue,
  line from Rect_(ri+hyi+h+g+hmo,start_tmp) to Rect_(ri+hyi+h+g,start_tmp)
  arcd(O,ri+hyi+h+g+hmo,start_tmp,end_tmp) ccw
  line from Rect_(ri+hyi+h+g,end_tmp) to Rect_(ri+hyi+h+g+hmo,end_tmp)
  arcd(O,ri+hyi+h+g,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

##upfacing magnets
#start_tmp = start_ang+(magnetPitch/2)+2*magnetPitch
#end_tmp = start_tmp+magnetPitch
#for i = 0 to 0 do{
  #rgbfill(lightred,
  #line from Rect_(ri+hyi+h+g+hmo,start_tmp) to Rect_(ri+hyi+h+g,start_tmp)
  #arcd(O,ri+hyi+h+g+hmo,start_tmp,end_tmp) ccw
  #line from Rect_(ri+hyi+h+g,end_tmp) to Rect_(ri+hyi+h+g+hmo,end_tmp)
  #arcd(O,ri+hyi+h+g,end_tmp,start_tmp) cw
  #)
  
  #start_tmp = start_tmp + 4*magnetPitch
  #end_tmp = end_tmp + 4*magnetPitch
#}


##DRAW RADIAL LINES FOR INNER MAGNETS AND OUTER MAGNETS
#for i = start_ang+(magnetPitch/2) to end_ang do{
  #line from Rect_(ri+hyi+h+g,i) to Rect_(ri+hyi+h+hmo+g,i)#outer rotor
  ##line from Rect_(ri,i) to Rect_(ri+hyi,i)#inner rotor
  
  #i = i + magnetPitch - 1
#}

##FILL COLOR FOR STATOR TEETH
#start_tmp = start_ang + coil_side_angle
#end_tmp = start_tmp+coil_block_angle
##for i = (start_ang+coil_side_angle) to (end_ang do-coil_side_angle){
#for i = 0 to 2 do{
  #rgbfill(lightgray,
  ##line from Rect_(ri,start_tmp) to Rect_(ri+hyi+h,start_tmp)
  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  ##arcd(O,ri+hyi+h,end_tmp,start_tmp) cw
  ##line from Rect_(ri+hyi+h,end_tmp) to Rect_(ri,end_tmp)
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  ##arcd(O,ri,start_tmp,end_tmp) ccw
  #arcd(O,ri,end_tmp,start_tmp) cw
  #)
  #start_tmp = start_tmp+coilPitch
  #end_tmp = end_tmp+coilPitch  
#}


#DRAW RADIAL AND TANGETAL LINES FOR STATOR COILS
j = 0
add = 0
for i = start_ang to end_ang do{
  temp = j%3
  if temp == 0 then {
    rgbfill(copper,
      line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      add = coil_side_angle
      arcd(O,ri+hyi+h,i+add,i) cw
      line from Rect_(ri+hyi+h,i+add) to Rect_(ri+hyi,i+add)
      arcd(O,ri+hyi,i,i+add) ccw
    )
    
    arcd(O,ri+hyi+0.5*h,i+add,i) cw
    
  }
  if temp == 1 then {
      line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      add = coil_block_angle
      arcd(O,ri+hyi+h,i,i+add) ccw
  }
  if temp == 2 && i+add-1 < end_ang then {
    rgbfill(copper,
      line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      add = coil_side_angle
      arcd(O,ri+hyi+h,i+add,i) cw
      line from Rect_(ri+hyi+h,i+add) to Rect_(ri+hyi,i+add)
      arcd(O,ri+hyi,i+add,i) cw
    )  
    
    arcd(O,ri+hyi+0.5*h,i+add,i) cw
  }
  
  
  i = i + add
  j = j + 1
  i = i - 1
}
add = coil_side_angle
rgbfill(copper,
  line from Rect_(ri+hyi,end_ang-add) to Rect_(ri+hyi+h,end_ang-add)
  arcd(O,ri+hyi+h,end_ang,end_ang-add) cw
  line from Rect_(ri+hyi+h,end_ang) to Rect_(ri+hyi,end_ang)
  arcd(O,ri+hyi,end_ang-add,end_ang) ccw
)
arcd(O,ri+hyi+0.5*h,end_ang,end_ang-add) cw



#line from Rect_(ri+hyi/2,start_ang) to Rect_(ri+hyi/2,start_ang+magnetPitch) ->

#setrgb(ivory3)
#DRAW INNER MAGNET ORIENTATION ARROWS
#j = 0
#for i = start_ang+(magnetPitch/4) to end_ang do{
  #temp = j%4
  
  #if temp == 0 then {
    #line from Rect_(ri+0.2*hyi,i+magnetPitch/4) to Rect_(ri+0.85*hyi,i+magnetPitch/4) <-
  #}  
  #if temp == 1 then {
    #arcd(O,ri+hyi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw ->
  #}
  #if temp == 2 then {
    #line from Rect_(ri+0.2*hyi,i+magnetPitch/4) to Rect_(ri+0.85*hyi,i+magnetPitch/4) ->
  #}
  #if temp == 3 then {
    #arcd(O,ri+hyi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  #}  
  
  #i = i - 1 + magnetPitch
  #j = j + 1
#}

#test lines
#rgbfill(copper,
#line from (-5,ri-hyi) to (5,ri-hyi)
#line from (5,ri-hyi) to (5,ri-hyi-5)
#line from (5,ri-hyi-5) to (-5,ri-hyi-5)
#line from (-5,ri-hyi-5) to (-5,ri-hyi)
#)

#\newgray{darkgray}{.99}
#\psset{linecolor={rgb:black,1;white,2},fillcolor={rgb:black,1;white,2}}
#\psset{linecolor=green,fillcolor=green}
#\psset{linecolor=darkgray,fillcolor=darkgray}


#\psset{linecolor=bwhite,fillcolor=bwhite}
##\psset{linecolor=black!60!green,fillcolor=black!60!green}
#rgbfill(copper,
##rgbfill(white,
#line from Rect_(ri-hyi,i) to Rect_(ri-hyi-h,i) #heading down (1)
#add = coil_side_angle
####arcd(O,ri-hyi-h,i,i+add) ccw #heading left (2)
#arcd(O,ri-hyi-h,i+add,i) cw #heading left (2)
####arcd(O,ri-hyi-h,i+add+5,i+5) cw #heading left (2)
#line from Rect_(ri-hyi-h,i+add) to Rect_(ri-hyi,i+add) #heading up (3)
####arcd(O,ri-hyi,i+add,i) cw #heading right (4) (but actually heading left)
#arcd(O,ri-hyi,i,i+add) ccw #heading right (4)
####arcd(O,ri-hyi,i+add+7,i+7) cw #heading right (4) (but actually heading left)
#)

#\psset{linecolor=bblack,fillcolor=bblack}

#rgbfill(copper,
#line from Rect_(ri-hyi,i) to Rect_(ri-hyi-h,i) #heading down (1)
#add = coil_side_angle
#arcd(O,ri-hyi-h,i+add,i) cw #heading left (2)
#line from Rect_(ri-hyi-h,i+add) to Rect_(ri-hyi,i+add) #heading up (3)
#arcd(O,ri-hyi,i,i+add) ccw #heading right (4)
#)

#DRAW OUTER MAGNET ORIENTATION ARROWS
#\psset{linecolor=lightgray,fillcolor=lightgray}
#\psset{linewidth = 0.1pt}
#arcd((1,-1),,0,-90,<- outlined "red", 10) dotted
#j = 0
##for i = start_ang+(magnetPitch/4) to end_ang do{
#magnet_arrow_height = 0.1
#for i = start_ang+3*(magnetPitch/4) to end_ang do{
  #temp = j%4
  
  #if temp == 0 then {
      ##line from Rect_(ri+hyi+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+g+0.85*hmo,i+magnetPitch/4) <- magnet_arrow_height
      #line from Rect_(ri+hyi+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+g+0.85*hmo,i+magnetPitch/4) <-
    ##rgbfill(copper,
      #arcd(O,ri+hyi+h+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
    ##)
  #}  
  ##if temp == 1 then {
    ##arcd(O,ri+hyi+g+h+g+hmo/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  ##}
  #if temp == 2 then {
    #line from Rect_(ri+hyi+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+g+0.85*hmo,i+magnetPitch/4) ->
    #arcd(O,ri+hyi+h+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
  #}
  ##if temp == 3 then {
    ##arcd(O,ri+hyi+h+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
  ##}  
  
  #i = i - 1 + magnetPitch
  #j = j + 1
#}
#\psset{linewidth = 1pt}
I_dot with .c at ((Rect_(ri+hyi+g+h+hmo/2,start_ang+(magnetPitch))))

#DRAW STATOR COIL CONDUCTORS
j = 0
add = 0
for i = start_ang to end_ang do{
  temp = j%3
  if temp == 0 then {
    add = coil_side_angle
    #I_dot with .c at ((Rect_(ri+hyi+0.9*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.8*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.7*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.6*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.4*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.3*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.2*h,i + coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.1*h,i + coil_side_angle/2)))
    
    I_cross with .c at ((Rect_(ri+hyi+0.8*h,i+coil_side_angle/2)))
    I_cross with .c at ((Rect_(ri+hyi+0.5*h,i+coil_side_angle/2)))
    I_cross with .c at ((Rect_(ri+hyi+0.2*h,i+coil_side_angle/2)))
  }
  if temp == 0 then {
    add = coil_block_angle
  }  
  if temp == 2 && i+add-1 < end_ang then {
    add = coil_side_angle
    I_dot with .c at ((Rect_(ri+hyi+0.8*h,i + coil_block_angle/2 - coil_side_angle/2)))
    I_dot with .c at ((Rect_(ri+hyi+0.5*h,i + coil_block_angle/2 - coil_side_angle/2)))
    I_dot with .c at ((Rect_(ri+hyi+0.2*h,i + coil_block_angle/2 - coil_side_angle/2)))
  }
  
  
  i = i + add - 1
  j = j + 1
  i = i - 1
}
#add = coil_side_angle
#I_dot with .c at ((Rect_(ri+hyi+0.9*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.8*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.7*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.6*h,end_ang - coil_side_angle/2)))
##I_dot with .c at ((Rect_(ri+hyi+0.5*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.4*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.3*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.2*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.1*h,end_ang - coil_side_angle/2)))


#line from O to Rect_(ri,end_ang) ->
#"$R_i$" at last line .end above rjust

#"test" at 1/3 between last line .end and 2nd last arc .c



.PE