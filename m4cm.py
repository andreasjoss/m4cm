#!/usr/bin/python
# -*- coding: utf-8 -*-

#Andreas Joss says, use like this in terminal to produce pdf:
#./m4cm.py -p file.m4 

#also make sure you are using this version of PStricks (and not the newer versions which are available)
#`PSTricks' v2.43  <2013/05/12> (tvz)

import os
import re
import sys
import glob
import shutil
import commands
import subprocess

from optparse import OptionParser

#------------------------------------------------------------------------------#
usage_str = "usage: %prog [options] M4CM_FILE.m4"

parser = OptionParser(usage=usage_str)
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
parser.add_option("-a", "--append",
                  action="store",  #stores argument (default action)
                  dest="append",   #name of the stored argument (i.e append chars)
                  type="string",   #stores argument as a "string" (default type)
                  nargs=1,         #number of arguments (default value)
                  metavar="APPEND_CHARS",
                  help="Character/String to append to the end of the output file(s)")
parser.add_option("-b", "--boundingbox_offset",
                  action="store",
                  dest="BB_offset",
                  type="int",
                  nargs=4,         #number of arguments (default value)
                  metavar="L T R B",
                  help="the BoundingBox Offset: left offset point value, top offset point value, right offset point value, bottom offset point values, the default is: 1 1 1 1, i.e. 1pt in each direction")
parser.add_option("-d", "--delete_eps",
                  action="store_true",
                  dest="DELETE_EPS",
                  help="delete EPS file after converting to PNG or PDF")
parser.add_option("-e", "--eps",
                  action="store_true",
                  dest="EPS",
                  help="convert to EPS format [default option]")
parser.add_option("-g", "--ghostscript",
                  action="store_true",
                  dest="GS",
                  help="use GhostScript to convert to PNG format")
parser.add_option("-j", "--jpg",
                  action="store_true",
                  dest="JPG",
                  help="convert to JPG format")
parser.add_option("-n", "--png",
                  action="store_true",
                  dest="PNG",
                  help="convert to PNG format")
parser.add_option("-o", "--output_dir",
                  action="store",
                  dest="output_dir",
                  type="string",
                  nargs=1,
                  metavar="OUTPUT_DIR",
                  help="the Output Directory for the EPS/PDF/PNG/JPG output files")
parser.add_option("-p", "--pdf",
                  action="store_true",
                  dest="PDF",
                  help="convert to PDF format")
parser.add_option("-q", "--quiet",
                  action="store_true",
                  dest="QUIET",
                  help="run quietly, but print progress")
parser.add_option("-Q", "--super_quiet",
                  action="store_true",
                  dest="SUPER_QUIET",
                  help="run quietly with no output")
parser.add_option("-r", "--resolution",
                  action="store",
                  dest="resolution",
                  type="int",      #stores argument as a "int"
                  nargs=1,         #number of arguments (default value)
                  metavar="PNG_RESOLUTION",
                  help="PNG resolution (or density)")
parser.add_option("-t", "--template",
                  action="store",  #stores argument (default action)
                  dest="template", #name of the stored argument (i.e template file)
                  type="string",   #stores argument as a "string" (default type)
                  nargs=1,         #number of arguments (default value)
                  metavar="LATEX_TEMPLATE_FILE",
                  help='LaTeX template file to be used [default="default.ltx"]')

parser.set_defaults(append="",BB_offset=(1,1,1,1),EPS=True,PNG=False,JPG=False,GS=False,PDF=False, resolution=600,template="default.ltx",output_dir="",QUIET=False)

(options, m4cmFile) = parser.parse_args()

#------------------------------------------------------------------------------#
try:
  (m4cmFileName, m4cmFileExt) = os.path.splitext(m4cmFile[0])
except:
  parser.print_help()
  parser.error('An OptionParser error occurred...!')

#------------------------------------------------------------------------------#
if not os.path.exists(options.template):
  parser.print_help()
  raise OSError, 'LATEX_TEMPLATE_FILE, %s, could not be found...' % options.template

if re.search(r"%dpic_pstricks_output_here%",file(options.template).read(),re.MULTILINE)==None:
  parser.print_help()
  raise 'The "%dpic_pstricks_output_here%" tag in the LATEX_TEMPLATE_FILE could not be found...'

#------------------------------------------------------------------------------#
if re.search(r'\*',m4cmFileName):
  globlist=glob.glob(m4cmFileName+m4cmFileExt)
  m4cmFileNameList=[]
  for each_m4cmFileName in globlist:
    m4cmFileNameList.append(os.path.splitext(each_m4cmFileName)[0])

else:
  m4cmFileNameList=[m4cmFileName]

for filename in m4cmFileNameList:
  if not options.SUPER_QUIET: print("\nProcessing: "+filename+m4cmFileExt)
  if os.path.exists(filename+m4cmFileExt):
    #Parsing with M4 Circuit Macros to generate PIC code
    if not options.SUPER_QUIET: print("m4cm -> pic")
    m4_cmd=["m4",filename+m4cmFileExt]
    filename=filename+options.append
    m4_subprocess=subprocess.Popen(m4_cmd,stdout=subprocess.PIPE)

    #Compiling the PIC code to pstricks output
    if not options.SUPER_QUIET: print("pic -> pstricks")
    dpic_cmd="dpic -p"
    dpic_subprocess=subprocess.Popen(dpic_cmd,stdin=m4_subprocess.stdout,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)

    #Wrapping M4CM_FILE with the LATEX_TEMPLATE_FILE
    if not options.SUPER_QUIET: print("wrapping the "+filename+".tex with "+options.template)
    tmpl_lst=file(options.template).readlines()
    dpic_out,dpic_err=dpic_subprocess.communicate()
    if dpic_err:
      raise NameError(dpic_err)
      break
    tex_lst=dpic_out.splitlines()
    for i in range(len(tmpl_lst)):
      if re.search(r"%dpic_pstricks_output_here%",tmpl_lst[i]):
        tmpl_lst.pop(i)
        for j in range(len(tex_lst)):
            tmpl_lst.insert(i+j,tex_lst[j]+'\n')
        break

    #Create Output Directory if required
    cwd=os.getcwd()
    if options.output_dir!="":
      if not os.path.exists(options.output_dir):
        os.mkdir(options.output_dir)
      os.chdir(options.output_dir)

    file(filename+".ltx","w").writelines(tmpl_lst)

    #Creating a DVI output using LaTeX
    if not options.SUPER_QUIET: print("pstricks -> dvi")
    latex_cmd="latex "
    if options.QUIET or options.SUPER_QUIET:
      if sys.platform=='win32':
        latex_cmd+="-quiet "
      else:
        latex_cmd+="-interaction=batchmode "
    latex_cmd+=filename+".ltx"
    if sys.platform!='win32' and options.SUPER_QUIET:
      latex_cmd+=" 1> /dev/null 2>&1"
    latex_err=os.system(latex_cmd)
    if latex_err:
      print('LaTeX found errors in "'+filename+'.ltx"')
      if options.QUIET or options.SUPER_QUIET:
        print('To find out what the problem was, run the program without the "-q"/"--quiet or -Q"/"--super_quiet option...\n')
      else:
        print('\n')
      sys.exit(1)

    #Delete all the LaTeX files
    if os.path.exists(filename+".aux"):
      os.remove(filename+".aux")
    if os.path.exists(filename+".log"):
      os.remove(filename+".log")
    if os.path.exists(filename+".out"):
      os.remove(filename+".out")
    if os.path.exists(filename+".ltx"):
      os.remove(filename+".ltx")

    #Converting the DVI output to PostScript
    if not options.SUPER_QUIET: print('dvi -> ps')
    if options.QUIET or options.SUPER_QUIET:
      dvips_cmd=['dvips','-j0','-G0','-Ppdf','-Pdownload35','-q',filename+'.dvi']
    else:
      dvips_cmd=['dvips','-j0','-G0','-Ppdf','-Pdownload35',filename+'.dvi']
    dvips_subprocess=subprocess.Popen(dvips_cmd,stderr=subprocess.PIPE)
    dvips_err=dvips_subprocess.communicate()[1]
    if dvips_err:
      print('DVIPS Error:')
      print(dvips_err)
      #break
    os.remove(filename+'.dvi')

    #Converting the PS file to an EPS file
    if not options.SUPER_QUIET: print('ps -> eps')
    gs_exe='gswin64c' if sys.platform=='win32' else 'gs' 
    command = '%s -dBATCH -dNOPAUSE -sDEVICE=bbox "%s"' %  (gs_exe, filename+'.ps')
    stdin, stdout, stderr = os.popen3(command)
    bbox_info = stderr.read()
    BB_match=re.search(r'(%%BoundingBox:)\s+([0-9]*)\s+([0-9]*)\s+([0-9]*)\s+([0-9]*)',bbox_info)
    HRBB_match=re.search(r'(%%HiResBoundingBox:)\s+([0-9\.]*)\s+([0-9\.]*)\s+([0-9\.]*)\s+([0-9\.]*)',bbox_info)
    ps_file_lst=file(filename+'.ps','r').readlines()    
    ps_file_lst.pop(0)
    ps_file_lst.insert(0,'%!PS-Adobe-3.0 EPSF-3.0\n')
    for line_no, line in enumerate(ps_file_lst):
      if re.search(r'%%BoundingBox:',line):    #Expand the BoundingBox by 1pt in each direction
        ps_file_lst.pop(line_no)
        ps_file_lst.insert(line_no,'''%s %d %d %d %d
%s %.6f %.6f %.6f %.6f\n''' % (BB_match.group(1), int(BB_match.group(2))-options.BB_offset[0],
                                                  int(BB_match.group(3))-options.BB_offset[1],
                                                  int(BB_match.group(4))+options.BB_offset[2],
                                                  int(BB_match.group(5))+options.BB_offset[3],
                               HRBB_match.group(1), float(HRBB_match.group(2))-options.BB_offset[0],
                                                    float(HRBB_match.group(3))-options.BB_offset[1],
                                                    float(HRBB_match.group(4))+options.BB_offset[2],
                                                    float(HRBB_match.group(5))+options.BB_offset[3]))
      elif re.search(r'%%EndComments',line):
        i=line_no
        ps_file_lst.insert(line_no,'''%%BeginProlog
save
countdictstack
mark
newpath
/showpage {} def
/setpagedevice {pop} def
%%EndProlog
%%Page 1 1\n''')
        break
#      elif re.search(r'%%Bound',line) or re.search(r'%%HiResBound',line) or re.search(r'%%DocumentMedia',line) or re.search(r'%%Pages',line) or re.search(r'%%PageBoundingBox',line):    
#        ps_file_lst.pop(line_no)
    #Insert just before the '%%EOF' line:
    i=len(ps_file_lst)-1
    ps_file_lst.insert(i,'''cleartomark
countdictstack
exch sub { end } repeat
restore\n''') 
  
    file(filename+'.eps','w').writelines(ps_file_lst)
    os.remove(filename+'.ps')

    #Creating a PNG output, if required
    if options.PNG:
      if not options.SUPER_QUIET: print('eps -> png')
      if options.GS:
        #Using Ghostsript
        #Checking for platform
        if sys.platform == 'win32':
          ps_exe =  'gswin64c'
        else:
          ps_exe = 'gs'
        if options.QUIET or options.SUPER_QUIET:
          pngwrite_cmd='%s -dQUIET -dBATCH -dNOPAUSE -dSAFER -dEPSCrop -sDEVICE=pngalpha -r600 -sOutputFile=%s.png %s.eps' % (ps_exe,filename,filename)
        else:
          pngwrite_cmd='%s -dBATCH -dNOPAUSE -dSAFER -dEPSCrop -sDEVICE=pngalpha -r600 -sOutputFile=%s.png %s.eps' % (ps_exe,filename,filename)
      else:
        #Using ImageMagick's convert instead of Ghostsript
        pngwrite_cmd='convert -density %d %s.eps %s.png' % (options.resolution,filename,filename)

      pngwrite_err=os.system(pngwrite_cmd)
      if pngwrite_err:
        print("Error executing: "+pngwrite_cmd)
        break

    #Creating a JPG output, if required
    if options.JPG:
      if not options.SUPER_QUIET: print('eps -> jpg')
      #Using ImageMagick's convert instead of Ghostsript
      jpgwrite_cmd='convert -density %d %s.eps %s.jpg' % (options.resolution,filename,filename)
      jpgwrite_err=os.system(jpgwrite_cmd)
      if jpgwrite_err:
        print("Error executing: "+jpgwrite_cmd)
        break

    #Creating a PDF output, if required
    if options.PDF:
      if not options.SUPER_QUIET: print('eps -> pdf')
      eps2pdf_cmd='epstopdf '+filename+'.eps'
      eps2pdf_err=os.system(eps2pdf_cmd)
      if eps2pdf_err:
        print("Error executing: "+eps2pdf_cmd)
        break

    if (options.PNG or options.JPG or options.PDF) and options.DELETE_EPS:
      os.remove(filename+'.eps')
    if not options.SUPER_QUIET:
      print('Done!\n')
  else:
    print(filename+".m4 was not found...?")
  os.chdir(cwd)
