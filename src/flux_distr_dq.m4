scale=25.4  #Scaling Factor set to [mm] if scale=25.4

cct_init

linethick_(0.5)

_mm=0.9  #For custum scaling

pi = atan2(0,-1)
dx=10

define(`I_cross',`[\
  I: circle rad _rad fill 1;
  line from 1/5 between I.ne and I.sw to 4/5 between I.ne and I.sw;
  line from 1/5 between I.nw and I.se to 4/5 between I.nw and I.se]' )

define(`I_dot',`[\
  I: circle rad _rad fill 1;
  command "`\pscustom[fillstyle=solid]{'"
    circle rad _rad/5 with .c at I.c
  command "`}%'"]' )

define(`lightblue',`0.53,0.808,0.98')
define(`lightred',`0.941,0.502,0.502')
define(`lightyellow',`1.0,1.0,0.8')  

\newrgbcolor{altblue}{0.53 0.808 0.98}
\newrgbcolor{altred}{0.941 0.502 0.502}
\newrgbcolor{altyellow}{1.0 0.859 0.345}

#Flux Amplitude/Frequency etc.
_Br1=1         #Amplitude
_f=50	        #Frequency [Hz]
_T=1/_f      #Period
_w=2*pi*_f    #Frequency [rad/s]
_theta0=90

_cycles=2     #Number of Cycles
_ph=0         #Phase A

_res=100      #Resolotion
_dt=_T/_res  #Step size

_xscale=140*_mm/(_cycles*_T)   #X-axis scaling factor [i.e. 150mm wide]
_yscale=40*_mm/_Br1              #Y-axis scaling factor [i.e.50mm high]

#Dot & Cross radius
_rad=(_T/36)*_xscale

#The PM poles info
_K_p=1/2  # Pole-arc to Pole-pitch ratio
_mag_offset=8*_mm # Magnet offset [mm]
_mag_thick=6*_mm  # Magnet thickness/height

#Draw dimensions
_tick_offset=2  #Tick offset
_tick_length=7

O: (0,0)

for _c = 0 to _cycles-1 do {
  #Draw PM pole-pair
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .n at ((_T/4+_T*_c)*_xscale,-_mag_offset)
  command "`}%'"
  "$\boldsymbol{\uparrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .n at ((_T*_c)*_xscale,-_mag_offset)
  command "`}%'"
  "$\boldsymbol{\rightarrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .n at ((_T/2+_T*_c)*_xscale,-_mag_offset)
  command "`}%'"
  "$\boldsymbol{\leftarrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .n at ((_T*3/4+_T*_c)*_xscale,-_mag_offset)
  command "`}%'"
  "$\boldsymbol{\downarrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .s at ((_T/4+_T*_c)*_xscale,_mag_offset)
  command "`}%'"
  "$\boldsymbol{\uparrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .s at ((_T*_c)*_xscale,_mag_offset)
  command "`}%'"
  "$\boldsymbol{\leftarrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .s at ((_T/2+_T*_c)*_xscale,_mag_offset)
  command "`}%'"
  "$\boldsymbol{\rightarrow}$" at last box .c
  command "`\pscustom[fillstyle=solid, linecolor=black, fillcolor=lightgray]{'"
    B: box wid _K_p*_T/2*_xscale ht _mag_thick with .s at ((_T*3/4+_T*_c)*_xscale,_mag_offset)
  command "`}%'"
  "$\boldsymbol{\downarrow}$" at last box .c
}

#Draw sinusoidal flux distribution
for _t = 0 to _cycles*_T by _dt do {
  x0=(_t)*_xscale
  x1=(_t+_dt)*_xscale
  y0=_Br1*sin(_w*_t)*_yscale
  y1=_Br1*sin(_w*(_t+_dt))*_yscale

  line from (x0,y0) to (x1,y1) thick 2*_mm
}

#Add labels
line from (_T*(1+120/360)*_xscale,_Br1*sin(_w*_T*(1+120/360))*_yscale) right 10 ->;"${\omega_{mech}}$" ljust
spline 0.7 from (_T*(1+90/360)*_xscale,_Br1*sin(_w*_T*(1+90/360))*_yscale) up 7 then right 7 <-;"$\hat{B}_{r_{1|PM}}\cos(p\theta-p\omega_{mech} t-\alpha)$" ljust


#Draw Axis + Labels
centerline_(from (_theta0/360*_T*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset)) to (_theta0/360*_T*_xscale,_Br1*_yscale+_mag_offset*1.5),thick 1)
if _alpha<0 then {
  "$d_{PM}$" ljust
} else {
  "$d_{PM}$" rjust
}
if _type==0 then {
  _ds_x_offset=(_alpha+90)/360*_T*_xscale
} else {
  if _type==1 then {
    _ds_x_offset=(_alpha+60)/360*_T*_xscale
  } else {
    _ds_x_offset=(_alpha+120)/360*_T*_xscale-_rad*_l
  }
}
centerline_(from (_ds_x_offset,-(_mag_offset*1.5+_mag_thick+_tick_offset)) to (_ds_x_offset,_Br1*_yscale+_mag_offset*1.5),thick 1)
if _alpha<0 then {
  "$d_{AR}$" rjust
} else {
  "$d_{AR}$" ljust
}
line from (-_rad*(_l+2)+_alpha/360*_T*_xscale,0) right _cycles*_T*_xscale+20 ->
"$\theta$" below

_toggle=1
#Draw sinusoidal flux distribution
for _t = 0 to _cycles*_T by _dt do {
  x0=(_t-_T/4)*_xscale+_ds_x_offset
  x1=(_t-_T/4+_dt)*_xscale+_ds_x_offset
  y0=_Br1/6*sin(_w*_t)*_yscale
  y1=_Br1/6*sin(_w*(_t+_dt))*_yscale

  if _toggle==1 then {
    line coloured "gray" from (x0,y0) to (x1,y1) thick 1*_mm
	_toggle=0
  } else {
    line invis from (x0,y0) to (x1,y1) 
	_toggle=1
  }
}
spline 0.7 from (_T*_xscale+_ds_x_offset,_Br1/6*_yscale) up 14 then left 7 <-;"$\hat{B}_{r_{2|AR}}\cos(p\theta-p\omega_{mech} t)$" rjust


#Draw Coils
for _c = 0 to _cycles*_kq*2-1 do {
  if (_type!= 0) then {
    #For Type I & II
    _ph=_c % 3
    if _ph==0 && _color==1 then {
      #Phase A
      \psset{linecolor=altred}
      \psset{fillcolor=altred}
    } else {
      if _ph==1 && _color==1 then {
        #Phase C
        \psset{linecolor=altblue}
        \psset{fillcolor=altblue}
      } else {
        #Phase B
        if _ph==2 && _color==1 then {
          \psset{linecolor=altyellow}
          \psset{fillcolor=altyellow}
        } else {
          \psset{linecolor=black}
          \psset{fillcolor=black}
        }
      }
    }
  } else {
    #For Type 0
    if _color==1 then {
      #Draw Phase A in red
      \psset{linecolor=altred}
      \psset{fillcolor=altred}
    } else {
      #Draw Phase A in black
      \psset{linecolor=black}
      \psset{fillcolor=black}
    }#endifelse
  }

  if _draw_BC==1 || _draw_BC==0 && _ph==0 then {
    for _i = 0 to _l-1 do {
      #Draw each layer
      if _type==2 then {
        Id: I_dot with .c at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale+(_i*_rad*2-_rad*2*((_l-1)/2)),0)
        Ic: I_cross   with .c at ((_T*_alpha/360+_T*(_c+1)/(_kq*2))*_xscale+(-_i*_rad*2-_rad*(_l+1)),0)
      } else {#_type==1 || _type==0
        Id: I_dot with .c at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale+(_i*_rad*2-_rad*2*((_l-1)/2)),0)
        Ic: I_cross   with .c at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2))*_xscale+(-_i*_rad*2+_rad*2*((_l-1)/2)),0)
      }
      if _l>1 then {
        sprintf("\tiny{%g}",_l-_i) at Id.n above
        sprintf("\tiny{%g}'",_l-_i) at Ic.n above
      }
    }
  }
  if _type==0 then {#Overlap phase A
    "$_a$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
    "$_{a'}$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2))*_xscale,-_rad*2)
  } else {
    if _ph==0 then {#Non-overlap phase A
      if _type==2 then {#Type II
        "$_a$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
        "$_{a'}$" at ((_T*_alpha/360+_T*(_c+1)/(_kq*2))*_xscale-_rad*2*_l,-_rad*2)
      } else {#Type I
        "$_a$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
        "$_{a'}$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2))*_xscale,-_rad*2)
      }
    } else {
      if _ph==1 then {#Non-overlap phase C
        if _type==2 then {#Type II
          "$_c$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
          "$_{c'}$" at ((_T*_alpha/360+_T*(_c+1)/(_kq*2))*_xscale-_rad*2*_l,-_rad*2)
        } else {#Type I
          "$_c$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
          "$_{c'}$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2))*_xscale,-_rad*2)
        }
      } else {#Non-overlap phase B
        if _type==2 then {#Type II
          "$_b$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
          "$_{b'}$" at ((_T*_alpha/360+_T*(_c+1)/(_kq*2))*_xscale-_rad*2*_l,-_rad*2)
        } else {#Type I
          "$_b$"    at ((_T*_alpha/360+_T*_c/(_kq*2))*_xscale,-_rad*2)
          "$_{b'}$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2))*_xscale,-_rad*2)
        }#end else
      }#end else
    }#end if
  }#end else
}#end for
if _draw_BC==1 && _type==0 then {
  #Draw Phases B & C for Type 0
  #Phase B
  if _color==1 then {
    #Draw Phase B in yellow
    \psset{linecolor=altyellow}
    \psset{fillcolor=altyellow}
  } else {
    #Draw Phase B in black
    \psset{linecolor=black}
    \psset{fillcolor=black}
  }#endifelse
  for _c = 0 to _cycles*_kq*2-1 do {
    #Draw coils
    for _i = 0 to _l-1 do {
      #Draw each layer
      Id: I_dot   with .c at ((_T*_alpha/360+_T*_c/(_kq*2)+_T/3)*_xscale+(_i*_rad*2-_rad*2*((_l-1)/2)),0)
      Ic: I_cross with .c at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2)+_T/3)*_xscale+(-_i*_rad*2+_rad*2*((_l-1)/2)),0)
      if _l>1 then {
        sprintf("\tiny{%g}",_l-_i) at Id.n above
        sprintf("\tiny{%g}'",_l-_i) at Ic.n above
      }#endif
    }#endfor
    "$_b$"    at ((_T*_alpha/360+_T*_c/(_kq*2)+_T/3)*_xscale,-_rad*2)
    "$_{b'}$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2)+_T/3)*_xscale,-_rad*2)
  }#endfor
  #Phase C
  if _color==1 then {
    #Draw Phase C in blue
    \psset{linecolor=altblue}
    \psset{fillcolor=altblue}
  } else {
    #Draw Phase C in black
    \psset{linecolor=black}
    \psset{fillcolor=black}
  }#endifelse
  for _c = 0 to _cycles*_kq*2-1 do {
    #Draw coils
    for _i = 0 to _l-1 do {
      #Draw each layer
      Ic: I_cross with .c at ((_T*_alpha/360+_T*_c/(_kq*2)+_T/6)*_xscale+(_i*_rad*2-_rad*2*((_l-1)/2)),0)
      Id: I_dot   with .c at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2)+_T/6)*_xscale+(-_i*_rad*2+_rad*2*((_l-1)/2)),0)
      if _l>1 then {
        sprintf("\tiny{%g}",_l-_i) at Id.n above
        sprintf("\tiny{%g}'",_l-_i) at Ic.n above
      }#endif
    }#endfor
    "$_{c'}$"    at ((_T*_alpha/360+_T*_c/(_kq*2)+_T/6)*_xscale,-_rad*2)
    "$_c$" at ((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2)+_T/6)*_xscale,-_rad*2)
  }#endfor
}#endif

\psset{linecolor=black}

#if _alpha>0 then {
  #dimension_(from (0,-(_mag_offset*1.5+_mag_thick)) to\
    #(_T*_alpha/360*_xscale,-(_mag_offset*1.5+_mag_thick)),\
    #-_tick_length,"$_\alpha$",14pt__,_tick_offset,->)}

if _l>1 then {
  dimension_(from ((_T*_alpha/360)*_xscale-(_rad*2*((_l)/2)),_mag_offset+_mag_thick) to\
    ((_T*_alpha/360)*_xscale+(_rad*2*((_l)/2)),_mag_offset+_mag_thick),\
    _tick_length,"$\tfrac{2\Delta}{q}$",22pt__,_tick_offset,<->) #+_T/(_kq*4)
}

#dimension_(from (((_T*_alpha/360+_T*_c/(_kq*2)+_T/3)*_xscale,-_rad*2)) to (((_T*_alpha/360+_T/(_kq*4)+_T*_c/(_kq*2)+_T/3)*_xscale,-_rad*2)),10,"$\tfrac{2\Delta}{q}$", dx,7,<->)

dimension_(from (_T*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset)) to\
  (3*_T/2*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset)),\
  -_tick_length,"$\tfrac{\pi}{p}$",22pt__,_tick_offset,<->)

if _alpha<0 then {
  dimension_(from (_ds_x_offset,-(_mag_offset*1.5+_mag_thick+_tick_offset)) to\
  (_T*90/360*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset)),\
  -_tick_length,"$\delta$",22pt__,_tick_offset,<->)
} else {
  if _alpha>0 then {
    dimension_(from (_T*90/360*_xscale,-(_mag_offset*1.5*1.5+_mag_thick+_tick_offset*2+_tick_length*2)) to\
    (_ds_x_offset,-(_mag_offset*1.5*1.5+_mag_thick+_tick_offset*2+_tick_length*2)),\
    -_tick_length,"$\delta$",22pt__,_tick_offset,<->)
  }
}

if _type==0 then {
  #Type 0 Dimensions
  dimension_(from (_T*_alpha/360*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)) to\
    ((_T/(_kq*4)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)),\
    -_tick_length,"$\tfrac{\pi}{q}$",20pt__,_tick_offset,<->)
  dimension_(from ((_T/(_kq*4)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)) to\
    ((_T/(_kq*2)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)),\
    -_tick_length,"$\tfrac{\pi}{q}$",20pt__,_tick_offset,<->)
} else {
  if _type==1 then {
    #Type I Dimensions
    dimension_(from (_T*_alpha/360*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)) to\
      ((_T/(_kq*4)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)),\
      -_tick_length,"$\tfrac{\pi}{Q}$",20pt__,_tick_offset,<->)
    dimension_(from ((_T/(_kq*4)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)) to\
      ((_T/(_kq*2)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)),\
      -_tick_length,"$\tfrac{\pi}{Q}$",20pt__,_tick_offset,<->)\
  } else {
    #Type II Dimensions
    dimension_(from (_T*_alpha/360*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)) to\
      ((_T/(_kq*2)+_T*_alpha/360)*_xscale,-(_mag_offset*1.5+_mag_thick+_tick_offset*2+_tick_length)),\
      -_tick_length,"$\tfrac{2\pi}{Q}$",20pt__,_tick_offset,<->)}
}

