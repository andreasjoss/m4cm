.PS
include(pstricks.m4)
include(libcct.m4)

scale=25.4

cct_init

dy=10
dx=10

mm=1

ri=50*mm
g=1*mm
hyi=5*mm
hyo=7*mm
hmo=5*mm
h=10*mm
h_shoe=2*mm
h_shoe_skew=1*mm #has to be smaller than h_shoe
w_shoe=2.5*mm
w_shoe_cheat=0.01*mm
wc=5*mm #single coil side width
h_hole=3.5*mm
hole_radius=1.25*mm
cheat_ang = 0.85

define(`midnight',`0.1, 0.1, 0.44')
define(`gold',`1,0.84,0')
define(`white',`1, 1, 1')
define(`copper',`0.85, 0.54, 0.40')
define(`ivory3',`0.80,0.80,0.75')
define(`black',`0,0,0')
define(`dark_gray',`0.45,0.45,0.45')
define(`lightblue',`0.53,0.808,0.98')
define(`lightgray',`0.69,0.769,0.871')
define(`lightred',`0.941,0.502,0.502')

\newgray{bwhite}{1}
\newgray{bblack}{0}
#\newgray{bgray}{0.69}{0.769}{0.871}
\newrgbcolor{bgray}{0.69 0.769 0.871}

pi = atan2(0,-1)
radius_ = ((h/2)/3)*0.9

define(`I_cross',`[\
  I: circle rad radius_ fill 1;
  line from 1/5 between I.ne and I.sw to 4/5 between I.ne and I.sw;
  line from 1/5 between I.nw and I.se to 4/5 between I.nw and I.se]' )

define(`I_dot',`[\
  I: circle rad radius_ fill 1;
  command "`\pscustom[fillstyle=solid]{'"
    circle rad 2/5 with .c at I.c
    circle rad 1/5 with .c at I.c
    circle rad 0.1 with .c at I.c
  command "`}%'"]' )
  


p = 14
kq = 0.5
coilTotal = kq * 3 * p
coilPitch = (360)/coilTotal
magnetTotal = 4*p
magnetPitch = (360)/magnetTotal

kc = 0.55
coil_side_angle = 0.5*kc*coilPitch
coil_block_angle = (1-kc)*coilPitch

#start_ang=63
#end_ang=117#start_ang+8*magnetPitch

start_ang=90-4*magnetPitch
end_ang=90+4*magnetPitch

O: (0,0)


pos_rotated_x_1 = 0
pos_rotated_y_1 = 0

pos_rotated_x_2 = 0
pos_rotated_y_2 = 0

pos_rotated_tooth_x_1 = 0
pos_rotated_tooth_y_1 = 0

pos_rotated_tooth_x_2 = 0
pos_rotated_tooth_y_2 = 0

shoe_tip_rotate_x = 0
shoe_tip_rotate_y = 0

#pos_rotated = 0
#Pos_rotated: (0,0)

#$1 is xy coordinate, $2 is angle (deg) to rotate by
#this macro (or function) is a pic macro, not an m4 macro
#block [] parenthesis are used to scope variables
define rotate {[
x_rot = $1.x*cos($2*pi_/180)-$1.y*sin($2*pi_/180)
y_rot = $1.x*sin($2*pi_/180)+$1.y*cos($2*pi_/180)
$3 := x_rot #pass argument back using a third variable, which is supplied when calling this function
$4 := y_rot #pass argument back using a third variable, which is supplied when calling this function
]}



#arcd(O,ri,start_ang,end_ang) ccw
#arcd(O,ri+hyi,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g+h,start_ang,end_ang) ccw
#arcd(O,ri+hyi+g+h+g,start_ang,end_ang) ccw

#http://mbreen.com/m4.html#unexpandingmacros
#define(`_set', `define(`$1[$2]', `$3')')
#define(`_get', `defn(`$1[$2]')')

coil_pitch = 17.5

#position variables needs to be declared with a capital letter!! These coordinates are in x,y coordinates
Coil_Side_IN_LB: (-wc,ri+hyi)
Coil_Side_IN_RB: (0,ri+hyi)
Coil_Side_IN_LT: (-wc,ri+hyi+h)
Coil_Side_IN_RT: (0,ri+hyi+h)

Coil_Side_OUT_LB: (0,ri+hyi)
Coil_Side_OUT_RB: (wc,ri+hyi)
Coil_Side_OUT_LT: (0,ri+hyi+h)
Coil_Side_OUT_RT: (wc,ri+hyi+h)


#WORKS
#line from Coil_Side_IN_LB to Coil_Side_IN_RB
#line from Coil_Side_IN_RB to Coil_Side_IN_RT
#line from Coil_Side_IN_RT to Coil_Side_IN_LT
#line from Coil_Side_IN_LT to Coil_Side_IN_LB

#line from vrot_(Coil_Side_IN_LB,30,30) to vrot_(Coil_Side_IN_RB,30,30)
#line from vrot_(1,1,0,0) to vrot_(3,3,30,30)
#line from rrot_(1,1,0) to rrot_(1,3,0) #WORKS

#WORKS but does not rotate correctly
#coil_angle = 0
#line from rrot_(Coil_Side_IN_LB.x-Here.x,Coil_Side_IN_LB.y-Here.y,coil_angle) to rrot_(Coil_Side_IN_RB.x-Here.x,Coil_Side_IN_RB.y-Here.y,coil_angle)
#line from rrot_(Coil_Side_IN_RB.x-Here.x,Coil_Side_IN_RB.y-Here.y,coil_angle) to rrot_(Coil_Side_IN_RT.x-Here.x,Coil_Side_IN_RT.y-Here.y,coil_angle)
#line from rrot_(Coil_Side_IN_RT.x-Here.x,Coil_Side_IN_RT.y-Here.y,coil_angle) to rrot_(Coil_Side_IN_LT.x-Here.x,Coil_Side_IN_LT.y-Here.y,coil_angle)
#line from rrot_(Coil_Side_IN_LT.x-Here.x,Coil_Side_IN_LT.y-Here.y,coil_angle) to rrot_(Coil_Side_IN_LB.x-Here.x,Coil_Side_IN_LB.y-Here.y,coil_angle)


#WORKS!!!
#rotate(Coil_Side_IN_LB,coil_angle,pos_rotated_x_1,pos_rotated_y_1)
#rotate(Coil_Side_IN_RB,coil_angle,pos_rotated_x_2,pos_rotated_y_2)
#line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

#rotate(Coil_Side_IN_RB,coil_angle,pos_rotated_x_1,pos_rotated_y_1)
#rotate(Coil_Side_IN_RT,coil_angle,pos_rotated_x_2,pos_rotated_y_2)
#line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

#rotate(Coil_Side_IN_RT,coil_angle,pos_rotated_x_1,pos_rotated_y_1)
#rotate(Coil_Side_IN_LT,coil_angle,pos_rotated_x_2,pos_rotated_y_2)
#line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

#rotate(Coil_Side_IN_LT,coil_angle,pos_rotated_x_1,pos_rotated_y_1)
#rotate(Coil_Side_IN_LB,coil_angle,pos_rotated_x_2,pos_rotated_y_2)
#line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

define draw_coil_side {
in_Nout = $6
coil_rotate = $5
rgbfill(copper,
  rotate($1,coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  rotate($2,coil_rotate,pos_rotated_x_2,pos_rotated_y_2)
  line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

  rotate($2,coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  rotate($4,coil_rotate,pos_rotated_x_2,pos_rotated_y_2)
  line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

  rotate($4,coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  rotate($3,coil_rotate,pos_rotated_x_2,pos_rotated_y_2)
  line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)

  rotate($3,coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  rotate($1,coil_rotate,pos_rotated_x_2,pos_rotated_y_2)
  line from (pos_rotated_x_1,pos_rotated_y_1) to (pos_rotated_x_2,pos_rotated_y_2)
)
###draw stator conductors
if in_Nout == 1 then {
  rotate((0.5*$1.x,$1.y+0.2*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_cross with .c at (pos_rotated_x_1,pos_rotated_y_1)

  rotate((0.5*$1.x,$1.y+0.5*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_cross with .c at (pos_rotated_x_1,pos_rotated_y_1)

  rotate((0.5*$1.x,$1.y+0.8*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_cross with .c at (pos_rotated_x_1,pos_rotated_y_1)
}
if in_Nout == 0 then {
  rotate((0.5*$2.x,$2.y+0.2*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_dot with .c at (pos_rotated_x_1,pos_rotated_y_1)

  rotate((0.5*$2.x,$2.y+0.5*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_dot with .c at (pos_rotated_x_1,pos_rotated_y_1)

  rotate((0.5*$2.x,$2.y+0.8*h),coil_rotate,pos_rotated_x_1,pos_rotated_y_1)
  I_dot with .c at (pos_rotated_x_1,pos_rotated_y_1)
}
###draw stator conductors
}


define draw_stator_tooth {
rotation = $1
#obtain coordinates of rotated coil sides
stator_teeth_LB_x = 0
stator_teeth_LB_y = 0
stator_teeth_RB_x = 0
stator_teeth_RB_y = 0
stator_teeth_LT_x = 0
stator_teeth_LT_y = 0
stator_teeth_RT_x = 0
stator_teeth_RT_y = 0

###tooth part
temp = (0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_OUT_RB,temp,stator_teeth_LB_x,stator_teeth_LB_y)
rotate(Coil_Side_OUT_RT,temp,stator_teeth_LT_x,stator_teeth_LT_y)

temp = (-0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_IN_LB,temp,stator_teeth_RB_x,stator_teeth_RB_y)
rotate(Coil_Side_IN_LT,temp,stator_teeth_RT_x,stator_teeth_RT_y)

#line from (stator_teeth_LB_x,stator_teeth_LB_y) to (stator_teeth_LT_x,stator_teeth_LT_y) #these lines are actually just drawing over existing coil side lines
#line from (stator_teeth_RB_x,stator_teeth_RB_y) to (stator_teeth_RT_x,stator_teeth_RT_y) #these lines are actually just drawing over existing coil side lines

colour_fill_point_01_x = stator_teeth_LB_x
colour_fill_point_01_y = stator_teeth_LB_y

colour_fill_point_02_x = stator_teeth_LT_x
colour_fill_point_02_y = stator_teeth_LT_y

colour_fill_point_03_x = stator_teeth_RB_x
colour_fill_point_03_y = stator_teeth_RB_y

colour_fill_point_04_x = stator_teeth_RT_x
colour_fill_point_04_y = stator_teeth_RT_y

#obtain extra coordinates needed for stator colour fill
colour_fill_point_18_x = 0
colour_fill_point_18_y = 0

colour_fill_point_19_x = 0
colour_fill_point_19_y = 0

colour_fill_point_20_x = 0
colour_fill_point_20_y = 0

colour_fill_point_21_x = 0
colour_fill_point_21_y = 0

temp = (-0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_IN_RB,temp,colour_fill_point_18_x,colour_fill_point_18_y)
rotate((Coil_Side_IN_RB.x,Coil_Side_IN_RB.y-hyi),temp,colour_fill_point_19_x,colour_fill_point_19_y)

temp = (0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_OUT_LB,temp,colour_fill_point_20_x,colour_fill_point_20_y)
rotate((Coil_Side_OUT_LB.x,Coil_Side_OUT_LB.y-hyi),temp,colour_fill_point_21_x,colour_fill_point_21_y)

#obtain extra coordinates needed for kc dimension
kc_point_01_x = 0
kc_point_01_y = 0

kc_point_02_x = 0
kc_point_02_y = 0

temp = (0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_OUT_RB.x,Coil_Side_OUT_RB.y+0.5*h),temp,kc_point_01_x,kc_point_01_y)

temp = (-0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_IN_LB.x,Coil_Side_IN_LB.y+0.5*h),temp,kc_point_02_x,kc_point_02_y)

###tooth part

###dummy lines to create colour fill
stator_yoke_L_x = 0
stator_yoke_L_y = 0
stator_yoke_R_x = 0
stator_yoke_R_y = 0

temp = (0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_OUT_RB.x,Coil_Side_OUT_RB.y-hyi),temp,stator_yoke_L_x,stator_yoke_L_y)

temp = (-0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_IN_LB.x,Coil_Side_IN_LB.y-hyi),temp,stator_yoke_R_x,stator_yoke_R_y)

#line from (stator_teeth_LB_x,stator_teeth_LB_y) to (stator_yoke_L_x,stator_yoke_L_y)
#line from (stator_teeth_RB_x,stator_teeth_RB_y) to (stator_yoke_R_x,stator_yoke_R_y)

colour_fill_point_05_x = stator_yoke_L_x
colour_fill_point_05_y = stator_yoke_L_y

colour_fill_point_06_x = stator_yoke_R_x
colour_fill_point_06_y = stator_yoke_R_y

#arc cw from (stator_yoke_L_x,stator_yoke_L_y) to (stator_yoke_R_x,stator_yoke_R_y) with .c at O
###dummy lines to create colour fill


###111
temp = (0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_OUT_RT,temp,pos_rotated_tooth_x_1,pos_rotated_tooth_y_1)
rotate((Coil_Side_OUT_RT.x-w_shoe,Coil_Side_OUT_RT.y+h_shoe_skew),temp,pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)
#line from (pos_rotated_tooth_x_1,pos_rotated_tooth_y_1) to (pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)

colour_fill_point_07_x = pos_rotated_tooth_x_1
colour_fill_point_07_y = pos_rotated_tooth_y_1

colour_fill_point_08_x = pos_rotated_tooth_x_2
colour_fill_point_08_y = pos_rotated_tooth_y_2

temp = (-0.5*coil_pitch)+1.0*rotation
rotate(Coil_Side_IN_LT,temp,pos_rotated_tooth_x_1,pos_rotated_tooth_y_1)
rotate((Coil_Side_IN_LT.x+w_shoe,Coil_Side_IN_LT.y+h_shoe_skew),temp,pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)
#line from (pos_rotated_tooth_x_1,pos_rotated_tooth_y_1) to (pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)

colour_fill_point_09_x = pos_rotated_tooth_x_1
colour_fill_point_09_y = pos_rotated_tooth_y_1

colour_fill_point_10_x = pos_rotated_tooth_x_2
colour_fill_point_10_y = pos_rotated_tooth_y_2
###111

####222
temp = (0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_OUT_RT.x-w_shoe,Coil_Side_OUT_RT.y+h_shoe_skew),temp,pos_rotated_tooth_x_1,pos_rotated_tooth_y_1)
rotate((Coil_Side_OUT_RT.x-w_shoe-w_shoe_cheat,Coil_Side_OUT_RT.y+h_shoe),temp,pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)
#line from (pos_rotated_tooth_x_1,pos_rotated_tooth_y_1) to (pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)

colour_fill_point_11_x = pos_rotated_tooth_x_1
colour_fill_point_11_y = pos_rotated_tooth_y_1

colour_fill_point_12_x = pos_rotated_tooth_x_2
colour_fill_point_12_y = pos_rotated_tooth_y_2

temp = (-0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_IN_LT.x+w_shoe,Coil_Side_IN_LT.y+h_shoe_skew),temp,pos_rotated_tooth_x_1,pos_rotated_tooth_y_1)
rotate((Coil_Side_IN_LT.x+w_shoe+w_shoe_cheat,Coil_Side_IN_LT.y+h_shoe),temp,pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)
#line from (pos_rotated_tooth_x_1,pos_rotated_tooth_y_1) to (pos_rotated_tooth_x_2,pos_rotated_tooth_y_2)

colour_fill_point_13_x = pos_rotated_tooth_x_1
colour_fill_point_13_y = pos_rotated_tooth_y_1

colour_fill_point_14_x = pos_rotated_tooth_x_2
colour_fill_point_14_y = pos_rotated_tooth_y_2
####222

###shoe arc
shoe_tip_distance = distance((Coil_Side_OUT_RT.x-w_shoe-w_shoe_cheat,Coil_Side_OUT_RT.y+h_shoe),O)
shoe_tip_angle = acos(((Coil_Side_OUT_RT.y+h_shoe)/shoe_tip_distance))*pi_/180

shoe_tip_x = Here.x
shoe_tip_y = Here.y

shoe_tip_rotate_x = 0
shoe_tip_rotate_y = 0
temp = 1.0*rotation
rotate((0,shoe_tip_distance),temp,shoe_tip_rotate_x,shoe_tip_rotate_y)
#arc ccw from Here to (shoe_tip_rotate_x,shoe_tip_rotate_y) with .c at O

colour_fill_point_15_x = shoe_tip_x
colour_fill_point_15_y = shoe_tip_y

colour_fill_point_16_x = shoe_tip_rotate_x
colour_fill_point_16_y = shoe_tip_rotate_y

temp = (0.5*coil_pitch)+1.0*rotation
rotate((Coil_Side_OUT_RT.x-w_shoe-w_shoe_cheat,Coil_Side_OUT_RT.y+h_shoe),temp,shoe_tip_x,shoe_tip_y)
#arc ccw from (shoe_tip_rotate_x,shoe_tip_rotate_y) to (shoe_tip_x,shoe_tip_y) with .c at O

colour_fill_point_17_x = shoe_tip_x
colour_fill_point_17_y = shoe_tip_y
###shoe arc

###stator tooth colour fill
\psset{linecolor=bgray,fillcolor=bgray}
rgbfill(lightgray,
line from (colour_fill_point_01_x,colour_fill_point_01_y) to (colour_fill_point_02_x,colour_fill_point_02_y)
line from (colour_fill_point_02_x,colour_fill_point_02_y) to (colour_fill_point_08_x,colour_fill_point_08_y)
line from (colour_fill_point_08_x,colour_fill_point_08_y) to (colour_fill_point_12_x,colour_fill_point_12_y)

arc cw from (colour_fill_point_12_x,colour_fill_point_12_y) to (colour_fill_point_16_x,colour_fill_point_16_y) with .c at O
arc cw from (colour_fill_point_16_x,colour_fill_point_16_y) to (colour_fill_point_14_x,colour_fill_point_14_y) with .c at O

line from (colour_fill_point_14_x,colour_fill_point_14_y) to (colour_fill_point_10_x,colour_fill_point_10_y)
line from (colour_fill_point_10_x,colour_fill_point_10_y) to (colour_fill_point_09_x,colour_fill_point_09_y)
line from (colour_fill_point_09_x,colour_fill_point_09_y) to (colour_fill_point_03_x,colour_fill_point_03_y)

line from (colour_fill_point_03_x,colour_fill_point_03_y) to (colour_fill_point_18_x,colour_fill_point_18_y)
line from (colour_fill_point_18_x,colour_fill_point_18_y) to (colour_fill_point_19_x,colour_fill_point_19_y)
arc ccw from (colour_fill_point_19_x,colour_fill_point_19_y) to (colour_fill_point_21_x,colour_fill_point_21_y) with .c at O
line from (colour_fill_point_21_x,colour_fill_point_21_y) to (colour_fill_point_20_x,colour_fill_point_20_y)
line from (colour_fill_point_20_x,colour_fill_point_20_y) to (colour_fill_point_01_x,colour_fill_point_01_y)

#line from (colour_fill_point_03_x,colour_fill_point_03_y) to (colour_fill_point_06_x,colour_fill_point_06_y)
#arc ccw from (colour_fill_point_06_x,colour_fill_point_06_y) to (colour_fill_point_05_x,colour_fill_point_05_y) with .c at O
#line from (colour_fill_point_05_x,colour_fill_point_05_y) to (colour_fill_point_01_x,colour_fill_point_01_y)
)
###stator tooth colour fill

####execute drawing of outlines of stator teeth
\psset{linecolor=bblack,fillcolor=bblack}
line from (colour_fill_point_01_x,colour_fill_point_01_y) to (colour_fill_point_02_x,colour_fill_point_02_y)
line from (colour_fill_point_02_x,colour_fill_point_02_y) to (colour_fill_point_08_x,colour_fill_point_08_y)
line from (colour_fill_point_08_x,colour_fill_point_08_y) to (colour_fill_point_12_x,colour_fill_point_12_y)

arc cw from (colour_fill_point_12_x,colour_fill_point_12_y) to (colour_fill_point_16_x,colour_fill_point_16_y) with .c at O
arc cw from (colour_fill_point_16_x,colour_fill_point_16_y) to (colour_fill_point_14_x,colour_fill_point_14_y) with .c at O

line from (colour_fill_point_14_x,colour_fill_point_14_y) to (colour_fill_point_10_x,colour_fill_point_10_y)
line from (colour_fill_point_10_x,colour_fill_point_10_y) to (colour_fill_point_09_x,colour_fill_point_09_y)
line from (colour_fill_point_09_x,colour_fill_point_09_y) to (colour_fill_point_03_x,colour_fill_point_03_y)

#line from (colour_fill_point_03_x,colour_fill_point_03_y) to (colour_fill_point_06_x,colour_fill_point_06_y)
#arc ccw from (colour_fill_point_06_x,colour_fill_point_06_y) to (colour_fill_point_05_x,colour_fill_point_05_y) with .c at O
#line from (colour_fill_point_05_x,colour_fill_point_05_y) to (colour_fill_point_01_x,colour_fill_point_01_y)
####execute drawing of outlines of stator teeth

###stator tooth hole
rgbfill(white,
shoe_tooth_hole_x = 0
shoe_tooth_hole_y = 0
temp = 1.0*rotation
rotate((0,shoe_tip_distance-h_hole),temp,shoe_tooth_hole_x,shoe_tooth_hole_y)
circle rad hole_radius with .c at (shoe_tooth_hole_x,shoe_tooth_hole_y)
)
###stator tooth hole
}

draw_stator_tooth(0.0)
draw_stator_tooth(1.0*coil_pitch)
draw_stator_tooth(-1.0*coil_pitch)
#draw_stator_tooth(0.5)
#draw_stator_tooth(-0.5)
#draw_stator_tooth(0.5*coil_pitch,0.5*coil_pitch,-0.5*coil_pitch,-0.5*coil_pitch)
#draw_stator_tooth(0.5*coil_pitch,0.5*coil_pitch,-0.5*coil_pitch,-0.5*coil_pitch)
#draw_stator_tooth(1.5*coil_pitch,1.5*coil_pitch,0.5*coil_pitch,0.5*coil_pitch)

#draw the actual coil sides
draw_coil_side(Coil_Side_IN_LB,Coil_Side_IN_RB,Coil_Side_IN_LT,Coil_Side_IN_RT,0.5*coil_pitch,1)
draw_coil_side(Coil_Side_OUT_LB,Coil_Side_OUT_RB,Coil_Side_OUT_LT,Coil_Side_OUT_RT,0.5*coil_pitch,0)

draw_coil_side(Coil_Side_IN_LB,Coil_Side_IN_RB,Coil_Side_IN_LT,Coil_Side_IN_RT,-0.5*coil_pitch,1)
draw_coil_side(Coil_Side_OUT_LB,Coil_Side_OUT_RB,Coil_Side_OUT_LT,Coil_Side_OUT_RT,-0.5*coil_pitch,0)

draw_coil_side(Coil_Side_IN_LB,Coil_Side_IN_RB,Coil_Side_IN_LT,Coil_Side_IN_RT,-1.5*coil_pitch,1)
draw_coil_side(Coil_Side_OUT_LB,Coil_Side_OUT_RB,Coil_Side_OUT_LT,Coil_Side_OUT_RT,1.5*coil_pitch,0)

########################
#DRAW STATOR COIL SIDES# (OLD)
########################
#for i = 0 to 6 do{
  #for j = 0 to 4 do{ #run through each coordinate point of the coil side rectangle
    #if j == 0 then {
      #line from (coil_side_IN_LB_x,coil_side_IN_LB_y) to (coil_side_IN_RB_x,coil_side_IN_RB_y)}
    #elif j == 1 then {
      #line from (coil_side_IN_RB_x,coil_side_IN_RB_y) to (coil_side_IN_RT_x,coil_side_IN_RT_y)}
    
    #line from (coil_side_IN_RT_x,coil_side_IN_RT_y) to (coil_side_IN_LT_x,coil_side_IN_LT_y)
    #line from (coil_side_IN_LT_x,coil_side_IN_LT_y) to (coil_side_IN_LB_x,coil_side_IN_LB_y)  
  
  
  
#line from (coil_side_IN_LB_x,coil_side_IN_LB_y) to (coil_side_IN_RB_x,coil_side_IN_RB_y)
#line from (coil_side_IN_RB_x,coil_side_IN_RB_y) to (coil_side_IN_RT_x,coil_side_IN_RT_y)
#line from (coil_side_IN_RT_x,coil_side_IN_RT_y) to (coil_side_IN_LT_x,coil_side_IN_LT_y)
#line from (coil_side_IN_LT_x,coil_side_IN_LT_y) to (coil_side_IN_LB_x,coil_side_IN_LB_y)

#start_tmp = start_ang + coil_side_angle
#end_tmp = start_tmp+coil_block_angle
#\psset{linecolor=bwhite,fillcolor=bwhite}
##for i = (start_ang+coil_side_angle) to (end_ang do-coil_side_angle){
#for i = 0 to 2 do{
  #rgbfill(lightgray,

  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)

  
  #)
  #start_tmp = start_tmp+coilPitch
  #end_tmp = end_tmp+coilPitch  
#}

##draw lines which close inner yoke
##\psset{linecolor=bwhite,fillcolor=bwhite}
#\psset{linecolor=bgray,fillcolor=bgray}
#rgbfill(lightgray,
#arcd(O,ri,start_ang,end_ang) ccw
##arcd(O,ri,end_ang,start_ang) cw
##line from Rect_(ri,end_ang) to Rect_(ri+hyi,end_ang)
#line from Rect_(ri+hyi,end_ang) to Rect_(ri,end_ang)
##arcd(O,ri+hyi,start_ang,end_ang) ccw
#arcd(O,ri+hyi,end_ang,start_ang) cw
#line from Rect_(ri,start_ang) to Rect_(ri+hyi,start_ang)
#)
########################


#############################
#FILL COLOR FOR STATOR TEETH# (OLD)
#############################
#start_tmp = start_ang + coil_side_angle
#end_tmp = start_tmp+coil_block_angle
#\psset{linecolor=bwhite,fillcolor=bwhite}
##for i = (start_ang+coil_side_angle) to (end_ang do-coil_side_angle){
#for i = 0 to 2 do{
  #rgbfill(lightgray,
  ##line from Rect_(ri,start_tmp) to Rect_(ri+hyi+h,start_tmp)
  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  ##arcd(O,ri+hyi+h,end_tmp,start_tmp) cw
  ##line from Rect_(ri+hyi+h,end_tmp) to Rect_(ri,end_tmp)
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  ##arcd(O,ri,start_tmp,end_tmp) ccw
  #arcd(O,ri,end_tmp,start_tmp) cw
  #)
  #start_tmp = start_tmp+coilPitch
  #end_tmp = end_tmp+coilPitch  
#}

##draw lines which close inner yoke
##\psset{linecolor=bwhite,fillcolor=bwhite}
#\psset{linecolor=bgray,fillcolor=bgray}
#rgbfill(lightgray,
#arcd(O,ri,start_ang,end_ang) ccw
##arcd(O,ri,end_ang,start_ang) cw
##line from Rect_(ri,end_ang) to Rect_(ri+hyi,end_ang)
#line from Rect_(ri+hyi,end_ang) to Rect_(ri,end_ang)
##arcd(O,ri+hyi,start_ang,end_ang) ccw
#arcd(O,ri+hyi,end_ang,start_ang) cw
#line from Rect_(ri,start_ang) to Rect_(ri+hyi,start_ang)
#)
#############################


############
#OUTER YOKE#
############
#draw lines which close outer yoke
\psset{linecolor=bwhite,fillcolor=bwhite}
rgbfill(lightgray,
#rgbfill(copper,
arcd(O,ri+hyi+h+h_shoe+hmo+g,start_ang,end_ang) ccw
#\psset{linecolor=bwhite,fillcolor=bwhite}
line from Rect_(ri+hyi+h+h_shoe+g+hmo,end_ang) to Rect_(ri+hyi+h+h_shoe+g+hmo+hyo,end_ang)
#\psset{linecolor=bblack,fillcolor=bblack}
arcd(O,ri+hyi+h+h_shoe+hmo+g+hyo,end_ang,start_ang) cw
#\psset{linecolor=bwhite,fillcolor=bwhite}
line from Rect_(ri+hyi+h+h_shoe+g+hmo,start_ang) to Rect_(ri+hyi+h+h_shoe+g+hmo+hyo,start_ang)
)
#\psset{linecolor=bblack,fillcolor=bblack}

#draw lines which should remain black
\psset{linecolor=bblack,fillcolor=bblack}
arcd(O,ri,start_ang-cheat_ang,end_ang+cheat_ang) ccw
arcd(O,ri+hyi+h+h_shoe+hmo+g,start_ang,end_ang) ccw
arcd(O,ri+hyi+h+h_shoe+hmo+g+hyo,end_ang,start_ang) cw

\psset{linecolor=bblack,fillcolor=bblack}
############


##blank out lines which were just drawn
#\psset{linecolor=bwhite,fillcolor=bwhite}
#line from Rect_(ri+hyi+h+g+hmo,end_ang) to Rect_(ri+hyi+h+g+hmo+hyo,end_ang)
#line from Rect_(ri+hyi+h+g+hmo,start_ang) to Rect_(ri+hyi+h+g+hmo+hyo,start_ang)
#\psset{linecolor=bblack,fillcolor=bblack}


#darrow init

#darc(center position, radius, start radians, end radians, dline thickness, arrowhead wid, arrowhead
#ht, terminals)

#darc(O, ri-10, start_ang, end_ang, linewid, 1, <-, 0)

##Flux Amplitude/Frequency etc.
#_Af=1         #Amplitude
#_f=50	        #Frequency [Hz]
#_Tf=1/_f      #Period
#_wf=2*pi*_f   #Frequency [rad/s]
#_cycles=2
#_xscale=140*mm/(_cycles*_Tf)   #X-axis scaling factor [i.e. 150mm wide]
#_yscale=40*mm/_Af              #Y-axis scaling factor [i.e.50mm high]
#_K_p=2/3	# Pole-arc to Pole-pitch ratio
#_offset=8*mm	# Magnet offset [mm]
#_thick=6*mm	# Magnet thickness/height

#box wid _K_p*_Tf/2*_xscale ht _thick with .nw at ((_K_p*_Tf/8+_Tf)*_xscale,-_offset)

#line from O to Rect_(ri+hyi+h+hmo,end_ang+5) ->
#"$R_o$" at last line .end above rjust

###############################  
#FILL OUTER MAGNETS WITH COLOR#
###############################
#downfacing magnets
start_tmp = start_ang+magnetPitch/2
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightblue,
  line from Rect_(ri+hyi+h+h_shoe+g+hmo,start_tmp) to Rect_(ri+hyi+h+h_shoe+g,start_tmp)
  arcd(O,ri+hyi+h+h_shoe+g+hmo,start_tmp,end_tmp) ccw
  line from Rect_(ri+hyi+h+h_shoe+g,end_tmp) to Rect_(ri+hyi+h+h_shoe+g+hmo,end_tmp)
  arcd(O,ri+hyi+h+h_shoe+g,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}

#upfacing magnets
start_tmp = start_ang+(magnetPitch/2)+2*magnetPitch
end_tmp = start_tmp+magnetPitch
for i = 0 to 1 do{
  rgbfill(lightred,
  line from Rect_(ri+hyi+h+h_shoe+g+hmo,start_tmp) to Rect_(ri+hyi+h+h_shoe+g,start_tmp)
  arcd(O,ri+hyi+h+h_shoe+g+hmo,start_tmp,end_tmp) ccw
  line from Rect_(ri+hyi+h+h_shoe+g,end_tmp) to Rect_(ri+hyi+h+h_shoe+g+hmo,end_tmp)
  arcd(O,ri+hyi+h+h_shoe+g,end_tmp,start_tmp) cw
  )
  
  start_tmp = start_tmp + 4*magnetPitch
  end_tmp = end_tmp + 4*magnetPitch
}
###############################

######################################
#DRAW OUTER MAGNET ORIENTATION ARROWS#
######################################
#\psset{linecolor=lightgray,fillcolor=lightgray}
#\psset{linewidth = 0.1pt}
#arcd((1,-1),,0,-90,<- outlined "red", 10) dotted
j = 0
#for i = start_ang+(magnetPitch/4) to end_ang do{
magnet_arrow_height = 0.1
for i = start_ang+3*(magnetPitch/4) to end_ang do{
  temp = j%4
  
  if temp == 0 then {
      #line from Rect_(ri+hyi+h+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+g+0.85*hmo,i+magnetPitch/4) <- magnet_arrow_height
      line from Rect_(ri+hyi+h+h_shoe+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+h_shoe+g+0.85*hmo,i+magnetPitch/4) <-
    #rgbfill(copper,
      arcd(O,ri+hyi+h+h_shoe+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
    #)
  }  
  #if temp == 1 then {
    #arcd(O,ri+hyi+g+h+g+hmo/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  #}
  if temp == 2 then {
    line from Rect_(ri+hyi+h+h_shoe+g+0.2*hmo,i+magnetPitch/4) to Rect_(ri+hyi+h+h_shoe+g+0.85*hmo,i+magnetPitch/4) ->
    arcd(O,ri+hyi+h+h_shoe+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
  }
  #if temp == 3 then {
    #arcd(O,ri+hyi+h+g,i-(magnetPitch/4),i+magnetPitch-(magnetPitch/4)) ccw
  #}  
  
  i = i - 1 + magnetPitch
  j = j + 1
}
#\psset{linewidth = 1pt}
######################################

#######################
#DRAW DIMENSION LABELS#
#######################
#dimension (linespec,offset,label, D|H|W|blank width,tic offset,arrowhead )
setrgb(dark_gray)

#dimension_(from O to (Rect_(ri,(end_ang+start_ang)/2)),0,"$r_i$", dx,5,->)
dimension_(from (Rect_(0.5*ri,(end_ang+start_ang)/2)) to (Rect_(ri,(end_ang+start_ang)/2)),0,"$r_i$", dx,5,->)

temp = 10
#dimension_(from O to (Rect_(ri+hyi+h+hmo+2*g,end_ang+temp)),0,"$r_o$", dx,5,->)
dimension_(from (Rect_(0.47*(ri+hyi+h+h_shoe+hmo+g),end_ang+temp+5)) to (Rect_(ri+hyi+h+h_shoe+hmo+g+hyo,end_ang+temp)),0,"$r_o$", dx,5,->)
arcd(O,ri+hyi+h+h_shoe+hmo+g+hyo,end_ang+0.6*temp,end_ang+1.4*temp) ccw

#dimension_(from (Rect_(ri+hyi+g+h+g,start_ang)) to (Rect_(ri+hyi+g+h+hmo+g,start_ang)),-10,"$h_{mo}$", dx,7,<->)
temp = 10
middle = 1.1*temp
arc_width = 0.35
line from Rect_(ri+hyi+h+h_shoe+g+hmo+0.8*hmo,start_ang-middle) to Rect_(ri+hyi+h+h_shoe+g+hmo,start_ang-middle) ->
line from Rect_(ri+hyi+h+h_shoe+g-0.8*hmo,start_ang-middle) to Rect_(ri+hyi+h+h_shoe+g,start_ang-middle) ->
"$h_{mo}$"at -0.1 between last line .end and 2nd last line above ljust
arcd(O,ri+hyi+h+h_shoe+g,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
arcd(O,ri+hyi+h+h_shoe+g+hmo,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#dimension_(from (Rect_(ri,start_ang)) to (Rect_(ri+hyi,start_ang)),-10,"$h_{mi}$", dx,7,<->)
temp = 10
middle = 1.1*temp
arc_width = 0.35
line from Rect_(ri+hyi+0.8*hmo,start_ang-middle) to Rect_(ri+hyi,start_ang-middle) ->
line from Rect_(ri-0.8*hmo,start_ang-middle) to Rect_(ri,start_ang-middle) ->
"$h_{yi}$"at -0.1 between last line .end and 2nd last line above ljust
arcd(O,ri+hyi,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw
arcd(O,ri,start_ang-(1-arc_width)*middle,start_ang-(1+arc_width)*middle) cw

#dimension_(from (Rect_(ri+hyi+h+hmo,start_ang)) to (Rect_(ri+hyi+h+hmo,start_ang+magnetPitch)),-5,"$k_{m}\frac{\pi}{p}$", dx,0,<->)
line from Rect_(ri+hyi+h+h_shoe+g+hmo,start_ang+magnetPitch/2) to Rect_(ri+hyi+h+h_shoe+g+hmo+0.3*hmo,start_ang+magnetPitch/2)
line from Rect_(ri+hyi+h+h_shoe+g+hmo,start_ang+magnetPitch+magnetPitch/2) to Rect_(ri+hyi+h+h_shoe+g+hmo+0.3*hmo,start_ang+magnetPitch+magnetPitch/2)
#"$k_{mo}\frac{\pi}{p}$"at 0 between last line .end and 2nd last line center ljust
"$k_{m}\frac{\pi}{p}$"at 0 between last line .end and 2nd last line center ljust
dimension_(from (Rect_(ri+hyi+h+h_shoe+g+hmo+0.4*hmo,start_ang+magnetPitch/2)) to (Rect_(ri+hyi+h+h_shoe+g+hmo+0.4*hmo,start_ang+magnetPitch+magnetPitch/2)),0,"", dx,0,<->)
#arcd(O,ri+hyi+g+h+g+hmo+0.4*hmo,start_ang-0.3*temp,start_ang) ccw ->

#line from Rect_(ri,start_ang) to Rect_(ri-0.8*hmo,start_ang)
#line from Rect_(ri,start_ang+magnetPitch) to Rect_(ri-0.8*hmo,start_ang+magnetPitch)
##"$k_{mi}\frac{\pi}{p}$"at 0.8 between last line .end and 2nd last line below rjust
#"$k_{m}\frac{\pi}{p}$"at 0.8 between last line .end and 2nd last line below rjust
#dimension_(from (Rect_(ri-0.4*hmo,start_ang)) to (Rect_(ri-0.4*hmo,start_ang+magnetPitch)),0,"", dx,0,<->)

#dimension_(from (Rect_(ri+hyi+g,end_ang)) to (Rect_(ri+hyi+g+h,end_ang)),5,"$h_{c}$", dx,-3,<->)
temp = 10
middle = 0.5*temp
#line from Rect_(ri+hyi+h+0.8*hmo,end_ang+1.15*middle) to Rect_(ri+hyi+h,end_ang+1.15*middle) ->
#line from Rect_(ri+hyi-0.8*hmo,end_ang+1.15*middle) to Rect_(ri+hyi,end_ang+1.15*middle) ->
#line from Rect_(ri+hyi+h,end_ang+1.15*middle) to Rect_(ri+hyi+h-0.8*hmo,end_ang+1.15*middle) ->
line from Rect_(ri+hyi,end_ang+1.15*middle) to Rect_(ri+hyi+h,end_ang+1.15*middle) <->
#"$h_{c}$" at 0.15 between last line .end and 2nd last line above rjust
#"$h_{c}$" at last line .start above ljust
"$h_{c}$" at last line .start below ljust
arcd(O,ri+hyi,end_ang+0.9*middle,end_ang+1.4*middle) ccw
arcd(O,ri+hyi+h,end_ang+0.9*middle,end_ang+1.4*middle) ccw

middle = -2*magnetPitch
line from Rect_(ri+hyi+h+h_shoe+g+hmo,end_ang+middle) to Rect_(ri+hyi+h+h_shoe+g+hmo+hyo,end_ang+middle) <->
#line from Rect_(ri+hyi+h+g+hmo+hyo+0.8*hmo,end_ang+middle) to Rect_(ri+hyi+h+g+hmo+hyo,end_ang+middle) ->
#line from Rect_(ri+hyi+h+g+hmo-0.8*hmo,end_ang+middle) to Rect_(ri+hyi+h+g+hmo,end_ang+middle) ->
#"$h_{yo}$" at 0.27 between last line .end and 2nd last line above rjust
"$h_{yo}$" at 0.26 between last line .end and 2nd last line above
#arcd(O,ri+hyi+g+hmo,end_ang+0.5*middle,end_ang+1.5*middle) ccw
#arcd(O,ri+hyi+h+g+hmo+hyo,end_ang+0.5*middle,end_ang+1.5*middle) ccw

#dimension_(from (Rect_(ri+hyi+h/2,start_ang+coil_side_angle)) to (Rect_(ri+hyi+h/2,start_ang+coil_side_angle+coil_block_angle)),0,"", 0,0,<->)
dimension_(from (kc_point_01_x,kc_point_01_y) to (kc_point_02_x,kc_point_02_y),0,"", 0,0,<->)
#"$k_{c}$" at 0.75 between last line .end and 2nd last line above
"$k_{c}$" at 0.75 between last line .end and 2nd last line below

#dimension_(from (Rect_(ri+hyi,start_ang)) to (Rect_(ri+hyi+g,start_ang)),-2,"", dx,0,<->)
##"$g_i$" at last line .end below ljust
#"$g$" at last line .end below ljust

#dimension_(from (Rect_(ri+hyi+h+h_shoe+w_shoe_cheat,start_ang)) to (Rect_(ri+hyi+h+h_shoe+g,start_ang)),-2,"", dx,0,<->)
#"$g$" at last line .end below ljust

g_angle = start_ang+0.75*coil_pitch
dimension_(from (Rect_(ri+hyi+h+h_shoe+w_shoe_cheat,g_angle)) to (Rect_(ri+hyi+h+h_shoe+g,g_angle)),-2,"", dx,0,<->)
"$g$" at last line .end above rjust

#dimension_(from (Rect_(ri+hyi+h+h_shoe_skew,end_ang-0.3*middle)) to (Rect_(ri+hyi+h+h_shoe,end_ang-0.3*middle)),-2,"", dx,1,<->)
#"$h_{s}$" at -0.6 between last line .end and 2nd last line above
#dimension_(from (colour_fill_point_08_x,colour_fill_point_08_y) to (colour_fill_point_12_x,colour_fill_point_12_y),-2,"", dx,1,<->)
#"$h_{s}$" at -0.6 between last line .end and 2nd last line above
new_colour_fill_point_08_x = 0
new_colour_fill_point_08_y = 0
new_colour_fill_point_12_x = 0
new_colour_fill_point_12_y = 0
temp = 2*coil_pitch#+3
rotate((colour_fill_point_08_x,colour_fill_point_08_y),temp,new_colour_fill_point_08_x,new_colour_fill_point_08_y) #bot arc
rotate((colour_fill_point_12_x,colour_fill_point_12_y),temp,new_colour_fill_point_12_x,new_colour_fill_point_12_y) #top arc

hs_dimension_point_T_x = 0
hs_dimension_point_T_y = 0
hs_dimension_point_B_x = 0
hs_dimension_point_B_y = 0
#temp = 2
temp = 5
rotate((new_colour_fill_point_08_x,new_colour_fill_point_08_y),temp,hs_dimension_point_B_x,hs_dimension_point_B_y) #bot arc
rotate((new_colour_fill_point_12_x,new_colour_fill_point_12_y),temp,hs_dimension_point_T_x,hs_dimension_point_T_y) #top arc
arc ccw from (new_colour_fill_point_08_x,new_colour_fill_point_08_y) to (hs_dimension_point_B_x,hs_dimension_point_B_y) with .c at O
arc ccw from (new_colour_fill_point_12_x,new_colour_fill_point_12_y) to (hs_dimension_point_T_x,hs_dimension_point_T_y) with .c at O

hs_arrow_dimension_end_point_T_x = 0
hs_arrow_dimension_end_point_T_y = 0
hs_arrow_dimension_end_point_B_x = 0
hs_arrow_dimension_end_point_B_y = 0
arrow_temp = 0.8*temp
rotate((new_colour_fill_point_08_x,new_colour_fill_point_08_y),arrow_temp,hs_arrow_dimension_end_point_B_x,hs_arrow_dimension_end_point_B_y) #bot arc
rotate((new_colour_fill_point_12_x,new_colour_fill_point_12_y),arrow_temp,hs_arrow_dimension_end_point_T_x,hs_arrow_dimension_end_point_T_y) #top arc

hs_bot_distance = distance((hs_arrow_dimension_end_point_B_x,hs_arrow_dimension_end_point_B_y),O)
hs_top_distance = distance((hs_arrow_dimension_end_point_T_x,hs_arrow_dimension_end_point_T_y),O)
dimension_arrow_angle = acos(((hs_arrow_dimension_end_point_T_x)/hs_top_distance))#*pi_/180

line from rect_(hs_top_distance+4,dimension_arrow_angle) to rect_(hs_top_distance,dimension_arrow_angle) ->
line from rect_(hs_bot_distance-4,dimension_arrow_angle) to rect_(hs_bot_distance,dimension_arrow_angle) ->

##line from rect_(polar_(colour_fill_point_08_x,colour_fill_point_08_y)) to rect_(polar_(hs_dimension_point_B_x,hs_dimension_point_B_y)) ->
"$h_{s}$" at 0.8 between last line .end and 2nd last line above ljust
#"$h_{s}$" at 1.9 between last line .end and 2nd last line center ljust
#"$h_{s}$" at 0.6 between last line .end and 2nd last line above
#"$h_{s}$" at last line .end above rjust

#dimension_(from (new_colour_fill_point_08_x,new_colour_fill_point_08_y) to (new_colour_fill_point_12_x,new_colour_fill_point_12_y),5,"", -2,-2,<->)
#"$h_{s}$" at last line .end above rjust

#dimension_(from (Rect_(ri+hyi+h+h_shoe-h_hole,90)) to (Rect_(ri+hyi+h+h_shoe,90)),-2,"", dx,1,<->)
dimension_(from (Rect_(ri+hyi+h+h_shoe-h_hole,90)) to (Rect_(ri+hyi+h+h_shoe,90)),-2,"", dx,0,<->)
#"$h_{h}$" at 0.2 between last line .end and 2nd last line above
"$h_{h}$" at -0.6 between last line .end and 2nd last line center rjust

theta_dimension_extend_top_scale = 6
theta_dimension_top_vector_x = (colour_fill_point_10_x-colour_fill_point_09_x)
theta_dimension_top_vector_y = (colour_fill_point_10_y-colour_fill_point_09_y)
theta_dimension_top_vector_magnitude = distance((colour_fill_point_10_x,colour_fill_point_10_y),(colour_fill_point_09_x,colour_fill_point_09_y))
theta_dimension_top_vector_x = theta_dimension_top_vector_x/theta_dimension_top_vector_magnitude
theta_dimension_top_vector_y = theta_dimension_top_vector_y/theta_dimension_top_vector_magnitude
theta_dimension_extend_top_point_x = colour_fill_point_10_x + theta_dimension_top_vector_x*theta_dimension_extend_top_scale
theta_dimension_extend_top_point_y = colour_fill_point_10_y + theta_dimension_top_vector_y*theta_dimension_extend_top_scale
line from (colour_fill_point_10_x,colour_fill_point_10_y) to (theta_dimension_extend_top_point_x,theta_dimension_extend_top_point_y)
theta_dimension_line_magnitude = distance((colour_fill_point_10_x,colour_fill_point_10_y),(theta_dimension_extend_top_point_x,theta_dimension_extend_top_point_y))

theta_dimension_coil_side_aux_x = 0
theta_dimension_coil_side_aux_y = 0
temp = -1.5*coil_pitch
rotate(Coil_Side_IN_RT,temp,theta_dimension_coil_side_aux_x,theta_dimension_coil_side_aux_y)

theta_dimension_bot_vector_x = (theta_dimension_coil_side_aux_x-colour_fill_point_09_x)
theta_dimension_bot_vector_y = (theta_dimension_coil_side_aux_y-colour_fill_point_09_y)
theta_dimension_bot_vector_magnitude = distance((theta_dimension_coil_side_aux_x,theta_dimension_coil_side_aux_y),(colour_fill_point_09_x,colour_fill_point_09_y))
theta_dimension_bot_vector_x = theta_dimension_bot_vector_x/theta_dimension_bot_vector_magnitude
theta_dimension_bot_vector_y = theta_dimension_bot_vector_y/theta_dimension_bot_vector_magnitude
#theta_dimension_extend_bot_scale = theta_dimension_bot_vector_magnitude/theta_dimension_line_magnitude
theta_dimension_extend_bot_scale = 8.7

#theta_dimension_extend_bot_point_x = theta_dimension_coil_side_aux_x + theta_dimension_bot_vector_x*theta_dimension_extend_bot_scale
#theta_dimension_extend_bot_point_y = theta_dimension_coil_side_aux_y + theta_dimension_bot_vector_y*theta_dimension_extend_bot_scale
#line from (theta_dimension_coil_side_aux_x,theta_dimension_coil_side_aux_y) to (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y)

theta_dimension_extend_bot_point_x = colour_fill_point_09_x + theta_dimension_bot_vector_x*theta_dimension_extend_bot_scale
theta_dimension_extend_bot_point_y = colour_fill_point_09_y + theta_dimension_bot_vector_y*theta_dimension_extend_bot_scale
line from (colour_fill_point_09_x,colour_fill_point_09_y) to (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y)

theta_dimension_arc_extend_top_angle = 4.5
theta_dimension_arc_extend_bot_angle = -55
theta_dimension_extend_TOP_point_x = 0
theta_dimension_extend_TOP_point_y = 0
theta_dimension_extend_BOT_point_x = 0
theta_dimension_extend_BOT_point_y = 0

rotate((theta_dimension_extend_top_point_x,theta_dimension_extend_top_point_y),theta_dimension_arc_extend_top_angle,theta_dimension_extend_TOP_point_x,theta_dimension_extend_TOP_point_y)
rotate((theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y),theta_dimension_arc_extend_bot_angle,theta_dimension_extend_BOT_point_x,theta_dimension_extend_BOT_point_y)
#\psset{linecolor=bgray,fillcolor=bgray}
arc ccw color "gray" from (theta_dimension_extend_BOT_point_x,theta_dimension_extend_BOT_point_y) to (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) ->
#arc ccw fill 0.1 from (theta_dimension_extend_BOT_point_x,theta_dimension_extend_BOT_point_y) to (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) ->
#arc ccw from (theta_dimension_extend_BOT_point_x,theta_dimension_extend_BOT_point_y) to (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) ->
arc cw color "gray" from (theta_dimension_extend_TOP_point_x,theta_dimension_extend_TOP_point_y) to (theta_dimension_extend_top_point_x,theta_dimension_extend_top_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) ->

#arcd(O,ri+hyi+h+h_shoe+g+hmo,start_tmp,end_tmp) ccw

#arc ccw from (theta_dimension_extend_BOT_point_x,theta_dimension_extend_BOT_point_y) to (theta_dimension_extend_TOP_point_x,theta_dimension_extend_TOP_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) <->
#arc ccw from (theta_dimension_extend_bot_point_x,theta_dimension_extend_bot_point_y) to (theta_dimension_extend_top_point_x,theta_dimension_extend_top_point_y) with .c at (colour_fill_point_09_x,colour_fill_point_09_y) <->

"$\theta_{s}$" at 0.3 between last line .end and 2nd last line center ljust

shoe_tip_width_left_x = 0
shoe_tip_width_left_y = 0
shoe_tip_width_left_bot_x = 0
shoe_tip_width_left_bot_y = 0
shoe_tip_width_right_x = 0
shoe_tip_width_right_y = 0
shoe_tip_width_right_bot_x = 0
shoe_tip_width_right_bot_y = 0

temp = 0.5*coil_pitch
rotate(Coil_Side_IN_LT,temp,shoe_tip_width_left_x,shoe_tip_width_left_y)
rotate(Coil_Side_IN_LB,temp,shoe_tip_width_left_bot_x,shoe_tip_width_left_bot_y)

rotate((Coil_Side_IN_LT.x+0.5*wc,Coil_Side_IN_LT.y),temp,shoe_tip_width_right_x,shoe_tip_width_right_y)
rotate((Coil_Side_IN_LB.x+0.5*wc,Coil_Side_IN_LB.y),temp,shoe_tip_width_right_bot_x,shoe_tip_width_right_bot_y)

shoe_tip_width_left_vector_x = (shoe_tip_width_left_x-shoe_tip_width_left_bot_x)
shoe_tip_width_left_vector_y = (shoe_tip_width_left_y-shoe_tip_width_left_bot_y)
shoe_tip_width_left_vector_magnitude = distance((shoe_tip_width_left_x,shoe_tip_width_left_y),(shoe_tip_width_left_bot_x,shoe_tip_width_left_bot_y))
shoe_tip_width_left_vector_x = shoe_tip_width_left_vector_x/shoe_tip_width_left_vector_magnitude
shoe_tip_width_left_vector_y = shoe_tip_width_left_vector_y/shoe_tip_width_left_vector_magnitude
shoe_tip_width_left_dimension_scale = 4
shoe_tip_width_right_dimension_scale = 4

shoe_tip_width_dimension_extend_point_left_x = shoe_tip_width_left_x + shoe_tip_width_left_vector_x*shoe_tip_width_left_dimension_scale
shoe_tip_width_dimension_extend_point_left_y = shoe_tip_width_left_y + shoe_tip_width_left_vector_y*shoe_tip_width_left_dimension_scale

shoe_tip_width_dimension_extend_point_right_x = shoe_tip_width_right_x + shoe_tip_width_left_vector_x*shoe_tip_width_left_dimension_scale
shoe_tip_width_dimension_extend_point_right_y = shoe_tip_width_right_y + shoe_tip_width_left_vector_y*shoe_tip_width_left_dimension_scale

line from (shoe_tip_width_left_x,shoe_tip_width_left_y) to (shoe_tip_width_dimension_extend_point_left_x,shoe_tip_width_dimension_extend_point_left_y)
line from (shoe_tip_width_right_x,shoe_tip_width_right_y) to (shoe_tip_width_dimension_extend_point_right_x,shoe_tip_width_dimension_extend_point_right_y)

dimension_(from (shoe_tip_width_dimension_extend_point_left_x,shoe_tip_width_dimension_extend_point_left_y) to (shoe_tip_width_dimension_extend_point_right_x,shoe_tip_width_dimension_extend_point_right_y),-0.5,"", dx,0,<->)
"$w_s$" at last line .end above rjust

#######################


setrgb(black)


  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  #arcd(O,ri,end_tmp,start_tmp) cw


##DRAW RADIAL LINES FOR INNER MAGNETS AND OUTER MAGNETS
#for i = start_ang+(magnetPitch/2) to end_ang do{
  #line from Rect_(ri+hyi+h+g,i) to Rect_(ri+hyi+h+hmo+g,i)#outer rotor
  ##line from Rect_(ri,i) to Rect_(ri+hyi,i)#inner rotor
  
  #i = i + magnetPitch - 1
#}

##FILL COLOR FOR STATOR TEETH
#start_tmp = start_ang + coil_side_angle
#end_tmp = start_tmp+coil_block_angle
##for i = (start_ang+coil_side_angle) to (end_ang do-coil_side_angle){
#for i = 0 to 2 do{
  #rgbfill(lightgray,
  ##line from Rect_(ri,start_tmp) to Rect_(ri+hyi+h,start_tmp)
  #line from Rect_(ri+hyi+h,start_tmp) to Rect_(ri,start_tmp)
  #arcd(O,ri+hyi+h,start_tmp,end_tmp) ccw
  ##arcd(O,ri+hyi+h,end_tmp,start_tmp) cw
  ##line from Rect_(ri+hyi+h,end_tmp) to Rect_(ri,end_tmp)
  #line from Rect_(ri,end_tmp) to Rect_(ri+hyi+h,end_tmp)
  ##arcd(O,ri,start_tmp,end_tmp) ccw
  #arcd(O,ri,end_tmp,start_tmp) cw
  #)
  #start_tmp = start_tmp+coilPitch
  #end_tmp = end_tmp+coilPitch  
#}







#################################################
#DRAW RADIAL AND TANGETAL LINES FOR STATOR COILS# (OLD)
#################################################
#j = 0
#add = 0
#for i = start_ang to end_ang do{
  #temp = j%3
  #if temp == 0 then {
    #rgbfill(copper,
      #line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      #add = coil_side_angle
      #arcd(O,ri+hyi+h,i+add,i) cw
      #line from Rect_(ri+hyi+h,i+add) to Rect_(ri+hyi,i+add)
      #arcd(O,ri+hyi,i,i+add) ccw
    #)
    
  #}
  #if temp == 1 then {
      #line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      #add = coil_block_angle
      #arcd(O,ri+hyi+h,i,i+add) ccw
  #}
  #if temp == 2 && i+add-1 < end_ang then {
    #rgbfill(copper,
      #line from Rect_(ri+hyi,i) to Rect_(ri+hyi+h,i)
      #add = coil_side_angle
      #arcd(O,ri+hyi+h,i+add,i) cw
      #line from Rect_(ri+hyi+h,i+add) to Rect_(ri+hyi,i+add)
      #arcd(O,ri+hyi,i+add,i) cw
    #)    
  #}
  
  
  #i = i + add
  #j = j + 1
  #i = i - 1
#}
#add = coil_side_angle
#rgbfill(copper,
  #line from Rect_(ri+hyi,end_ang-add) to Rect_(ri+hyi+h,end_ang-add)
  #arcd(O,ri+hyi+h,end_ang,end_ang-add) cw
  #line from Rect_(ri+hyi+h,end_ang) to Rect_(ri+hyi,end_ang)
  #arcd(O,ri+hyi,end_ang-add,end_ang) ccw
#)
#################################################


#line from Rect_(ri+hyi/2,start_ang) to Rect_(ri+hyi/2,start_ang+magnetPitch) ->

#setrgb(ivory3)
#DRAW INNER MAGNET ORIENTATION ARROWS
#j = 0
#for i = start_ang+(magnetPitch/4) to end_ang do{
  #temp = j%4
  
  #if temp == 0 then {
    #line from Rect_(ri+0.2*hyi,i+magnetPitch/4) to Rect_(ri+0.85*hyi,i+magnetPitch/4) <-
  #}  
  #if temp == 1 then {
    #arcd(O,ri+hyi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw ->
  #}
  #if temp == 2 then {
    #line from Rect_(ri+0.2*hyi,i+magnetPitch/4) to Rect_(ri+0.85*hyi,i+magnetPitch/4) ->
  #}
  #if temp == 3 then {
    #arcd(O,ri+hyi/2,i-magnetPitch/7,i+magnetPitch/1.5) ccw <-
  #}  
  
  #i = i - 1 + magnetPitch
  #j = j + 1
#}

#test lines
#rgbfill(copper,
#line from (-5,ri-hyi) to (5,ri-hyi)
#line from (5,ri-hyi) to (5,ri-hyi-5)
#line from (5,ri-hyi-5) to (-5,ri-hyi-5)
#line from (-5,ri-hyi-5) to (-5,ri-hyi)
#)

#\newgray{darkgray}{.99}
#\psset{linecolor={rgb:black,1;white,2},fillcolor={rgb:black,1;white,2}}
#\psset{linecolor=green,fillcolor=green}
#\psset{linecolor=darkgray,fillcolor=darkgray}


#\psset{linecolor=bwhite,fillcolor=bwhite}
##\psset{linecolor=black!60!green,fillcolor=black!60!green}
#rgbfill(copper,
##rgbfill(white,
#line from Rect_(ri-hyi,i) to Rect_(ri-hyi-h,i) #heading down (1)
#add = coil_side_angle
####arcd(O,ri-hyi-h,i,i+add) ccw #heading left (2)
#arcd(O,ri-hyi-h,i+add,i) cw #heading left (2)
####arcd(O,ri-hyi-h,i+add+5,i+5) cw #heading left (2)
#line from Rect_(ri-hyi-h,i+add) to Rect_(ri-hyi,i+add) #heading up (3)
####arcd(O,ri-hyi,i+add,i) cw #heading right (4) (but actually heading left)
#arcd(O,ri-hyi,i,i+add) ccw #heading right (4)
####arcd(O,ri-hyi,i+add+7,i+7) cw #heading right (4) (but actually heading left)
#)

#\psset{linecolor=bblack,fillcolor=bblack}

#rgbfill(copper,
#line from Rect_(ri-hyi,i) to Rect_(ri-hyi-h,i) #heading down (1)
#add = coil_side_angle
#arcd(O,ri-hyi-h,i+add,i) cw #heading left (2)
#line from Rect_(ri-hyi-h,i+add) to Rect_(ri-hyi,i+add) #heading up (3)
#arcd(O,ri-hyi,i,i+add) ccw #heading right (4)
#)


#############################
#DRAW STATOR COIL CONDUCTORS# (OLD)
#############################
#j = 0
#add = 0
#for i = start_ang to end_ang do{
  #temp = j%3
  #if temp == 0 then {
    #add = coil_side_angle
    #I_cross with .c at ((Rect_(ri+hyi+0.8*h,i+coil_side_angle/2)))
    #I_cross with .c at ((Rect_(ri+hyi+0.5*h,i+coil_side_angle/2)))
    #I_cross with .c at ((Rect_(ri+hyi+0.2*h,i+coil_side_angle/2)))
  #}
  #if temp == 0 then {
    #add = coil_block_angle
  #}  
  #if temp == 2 && i+add-1 < end_ang then {
    #add = coil_side_angle
    #I_dot with .c at ((Rect_(ri+hyi+0.8*h,i + coil_block_angle/2 - coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.5*h,i + coil_block_angle/2 - coil_side_angle/2)))
    #I_dot with .c at ((Rect_(ri+hyi+0.2*h,i + coil_block_angle/2 - coil_side_angle/2)))
  #}
  
  
  #i = i + add - 1
  #j = j + 1
  #i = i - 1
#}
##add = coil_side_angle
#I_dot with .c at ((Rect_(ri+hyi+0.8*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.5*h,end_ang - coil_side_angle/2)))
#I_dot with .c at ((Rect_(ri+hyi+0.2*h,end_ang - coil_side_angle/2)))
#############################

#line from O to Rect_(ri,end_ang) ->
#"$R_i$" at last line .end above rjust

#"test" at 1/3 between last line .end and 2nd last arc .c



.PE