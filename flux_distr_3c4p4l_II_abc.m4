.PS
include(libcct.m4)
include(pstricks.m4)

#############
# Coil info #
#############
_alpha=-25   #Coil centre offset [degrees]
_kq=3/4     #Coils per pole
_l=4        #Number of layers
_draw_BC=1  #Draw phases B & C as well (True=1/False=0)
_type=2     #Type of winding (1="equally spaced", 2="as wide as possible")
_color=1    #Draw Phases in Color

include(src/flux_distr_dq.m4)

.PE